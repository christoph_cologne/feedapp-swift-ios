//
//  Tesseract.h
//  Tesseract
//
//  Created by Christoph Bohnen on 06.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Tesseract.
FOUNDATION_EXPORT double TesseractVersionNumber;

//! Project version string for Tesseract.
FOUNDATION_EXPORT const unsigned char TesseractVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Tesseract/PublicHeader.h>


#import <Tesseract/TesseractOCR.h>
#import <Tesseract/G8Tesseract.h>
#import <Tesseract/G8RecognizedBlock.h>
#import <Tesseract/G8TesseractParameters.h>
#import <Tesseract/G8RecognitionOperation.h>
#import <Tesseract/G8Constants.h>
#import <Tesseract/UIImage+G8Filters.h>