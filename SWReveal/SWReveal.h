//
//  SWReveal.h
//  SWReveal
//
//  Created by Christoph Bohnen on 06.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SWReveal.
FOUNDATION_EXPORT double SWRevealVersionNumber;

//! Project version string for SWReveal.
FOUNDATION_EXPORT const unsigned char SWRevealVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SWReveal/PublicHeader.h>

#import <SWReveal/SWRevealViewController.h>
