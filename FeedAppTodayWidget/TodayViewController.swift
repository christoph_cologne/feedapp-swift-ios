//
//  TodayViewController.swift
//  TodayWidget
//
//  Created by Christoph Bohnen on 20.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate, NCWidgetProviding {
    
    var products : [Product] = []
    let widgetTitle = NSLocalizedString("TODAY WIDGET TITLE", comment: "the title for the widget")
    
    
    @IBOutlet var collectionView: UICollectionView!
    
    @IBOutlet var widgetLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view from its nib.
        
        self.widgetLabel.text = widgetTitle
        
        collectionView.backgroundColor = UIColor.clearColor()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)) {
        let sharedDefaults = NSUserDefaults(suiteName: "group.christophbohnen.FeedApp")
        
        let productsData = sharedDefaults?.objectForKey("products") as! NSData
        products = NSKeyedUnarchiver.unarchiveObjectWithData(productsData) as! [Product]
        
        
        collectionView.reloadData()
        completionHandler(NCUpdateResult.NewData)
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("widgetCell", forIndexPath: indexPath) as! WidgetCollectionViewCell
        
        cell.nameLabel.text = products[indexPath.row].getName()
        cell.imageView.image = products[indexPath.row].getImages()[0] as UIImage
        return cell
        
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let url = NSURL(string: "FeedApp://product?1")
        self.extensionContext?.openURL(url!, completionHandler: { (finished) -> Void in
            
        
        })
        
        
    }
    
    
    
    
}
