//
//  ProductsCollectionView.swift
//  feedApp
//
//  Created by Christoph Bohnen on 24.03.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit


class ProductsCollectionView: UICollectionView,UICollectionViewDelegate,UICollectionViewDataSource {

    var products:[Product] = []
    var listLayout:ListViewFlowLayout!
    var gridLayout:GridViewFlowLayout!
    var gridLandscapeLayout:GridViewLandscapeFlowLayout!
    var productsViewControllerDelegate:ProductsViewControllerDelegate!
    
    var cellIdentifier = "ProductCell"
    
    
    struct Dimensions {
        static let GapWidth:CGFloat = 4
        static let VisibleItemsInY:CGFloat = 2.5
        static let LineDistance:CGFloat = 5
        static let SectionBottomInset:CGFloat = 20
        static let SectionTopInset:CGFloat = 10
        
        static let ModalWidth:CGFloat = 200
        static let ModalHeight:CGFloat = 200
    }
    
    
    lazy var refreshControl:UIRefreshControl! = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.whiteColor()
        refreshControl.attributedTitle = NSAttributedString(string: "Reload", attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
     
        return refreshControl
        }()
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.delegate = self
        self.dataSource = self
        self.addSubview(refreshControl)
        self.registerNib(UINib(nibName: "ProductionCollectionViewList", bundle: nil), forCellWithReuseIdentifier: "ProductCellList")

        
     
    }
 
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        return products.count
    }
 
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(self.cellIdentifier, forIndexPath: indexPath) as! ProductsCollectionViewCell
        
        
        //
        //         get the current product to be used in cell
        //
        
        let currentProduct = products[indexPath.row]
        
        
        //
        //         set the contents of labels in cell
        //
        
        cell.productPrice.text = currentProduct.priceAsStringWithCurrency()
        cell.productName.text = currentProduct.getName()
        cell.productDescription.text = currentProduct.getFirstImgURL()
        
        //
        //         request image if there is a string for a URL set
        //
        
        let imageURI = currentProduct.getImgURL(0)
    
            
        currentProduct.getImage([currentProduct.getFirstImgURL()!], callback: { (image,url) -> Void in
            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                
                //  check if its still the right image
                
                if ((url[0] as String) ==  imageURI ){
                    cell.imageView.image = image
                }
                
            })
        })
        
        
        return cell
    }
    
    
        //
        //         on click cell event
        //
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        
     
        
        
        
            self.productsViewControllerDelegate.segueToDetails(indexPath)
        
    }
    
    

}
