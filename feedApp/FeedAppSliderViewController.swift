//
//  FeedAppSliderViewController.swift
//  FeedApp
//
//  Created by Christoph Bohnen on 21.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit

class FeedAppSliderViewController: UIViewController {

    @IBOutlet var backgroundView: UIView!
    @IBOutlet var slider: UISlider!
    @IBOutlet var backButton: UIButton!
    
    var speedOMeter : SpeedOMeter!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        speedOMeter = SpeedOMeter(frame: CGRect(origin: CGPointZero, size: backgroundView.frame.size))
        backgroundView.addSubview(speedOMeter)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func sliderValueChanged(sender: UISlider) {
        
        speedOMeter.moveToValue(CGFloat(sender.value))
        speedOMeter.displayLabel.text = Formatter.sharedInstance.percentFormatter.stringFromNumber(sender.value)
        
        
    }

    @IBAction func dismissAction(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }
}
