//
//  BarcodeScanner.swift
//
//
//  Created by Christoph Bohnen on 19.03.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit
import AVFoundation


class BarcodeScanner:AVManager,AVCaptureMetadataOutputObjectsDelegate{
    
    
    var running = false
    var metaOutput : AVCaptureMetadataOutput!
    let shapeView = CAShapeLayer()
    
    let indicatorRectStartSize : CGFloat = 150
    let alertController = UIAlertController(title: "Barcode Scanner", message:
        "", preferredStyle: UIAlertControllerStyle.Alert)
    
    
    let barCodeTypes = [AVMetadataObjectTypeUPCECode,
        AVMetadataObjectTypeCode39Code,
        AVMetadataObjectTypeCode39Mod43Code,
        AVMetadataObjectTypeEAN13Code,
        AVMetadataObjectTypeEAN8Code,
        AVMetadataObjectTypeCode93Code,
        AVMetadataObjectTypeCode128Code,
        AVMetadataObjectTypePDF417Code,
        AVMetadataObjectTypeQRCode,
        AVMetadataObjectTypeAztecCode,
        AVMetadataObjectTypeInterleaved2of5Code,
        AVMetadataObjectTypeDataMatrixCode,
        AVMetadataObjectTypeITF14Code
        
    ]
    
    
    
    init(frame:CGRect,parentViewController:UIViewController){
        
        super.init(frame:frame,parentViewController:parentViewController,shallStartOnInit:true)
        setupSession()
        
    }
    
    
    
    
    func setupSession(){
        
        self.metaOutput  = AVCaptureMetadataOutput()
        self.metaOutput.setMetadataObjectsDelegate(self, queue: SECOND_QUEUE)
        //            self.metaOutput.metadataObjectTypes = self.barCodeTypes as [AnyObject]!
        
        
        
        if(self.captureSession.canAddOutput(self.metaOutput)){
            self.captureSession.addOutput(self.metaOutput)
            print("output added")
        }
        
        self.metaOutput.metadataObjectTypes = self.metaOutput.availableMetadataObjectTypes
        
        
        // MARK: - add alertaction
        alertController.view.tintColor = UIColor.blackColor()
        
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("END SCANNING", comment: "button that quits scanning"), style: UIAlertActionStyle.Destructive,handler: { (action) -> Void in
            self.parentViewController.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("CANCEL DISPLAYED BARCODE", comment: "button cancels displaying of actionsheet"), style: UIAlertActionStyle.Cancel, handler: {(action) -> Void in
            self.startRunning()
        }))
        
        
        
        
        
        
        // MARK: - build indicators
        
        let startRect = UIBezierPath(rect: CGRect(x: ((self.previewLayer.frame.width) / 2) - (indicatorRectStartSize / 2), y: ((self.previewLayer.frame.height) / 2) - (indicatorRectStartSize / 2) - 20, width: indicatorRectStartSize, height: indicatorRectStartSize))
        
        
        shapeView.fillRule = kCAFillRuleEvenOdd
        shapeView.strokeColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 0.4).CGColor
        shapeView.fillColor = UIColor.clearColor().CGColor
        
        shapeView.path = startRect.CGPath
        self.previewLayer.addSublayer(shapeView)
        
        
        
    }
    //    MARK: - Delegatefunction for AVCaptureMetadata
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        
        
        var barCodeObject : AVMetadataObject!
        var detectedString : String!
        if metadataObjects.count != 0{
            if let metadata = metadataObjects[0] as? AVMetadataMachineReadableCodeObject {
                for barcodeType in barCodeTypes {
                    
                    if metadata.type == barcodeType {
                        barCodeObject = self.previewLayer.transformedMetadataObjectForMetadataObject(metadata as AVMetadataMachineReadableCodeObject)
                        
                        detectedString = metadata.stringValue
                        
                        self.stopRunning()
                        break
                    }
                    
                }
            
        }
            
        print(metadataObjects)
            print("string is :\(detectedString), queue is : \(NSOperationQueue.currentQueue())")
            
            if (detectedString != nil){
                
                NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                    
                    self.alertController.message = String.localizedStringWithFormat(NSLocalizedString("BARCODE FOUND",comment: "is displayed when a Barcode is found"), detectedString)
                    self.parentViewController.presentViewController(self.alertController, animated: true, completion: nil)
                                        
                })
                
            }
            
        }
        
    }
    
    
    
    
}