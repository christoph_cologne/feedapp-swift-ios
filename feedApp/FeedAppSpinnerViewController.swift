//
//  FeedAppSpinnerViewController.swift
//  FeedApp
//
//  Created by Christoph Bohnen on 22.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit

class FeedAppSpinnerViewController: UIViewController {

    @IBOutlet var backButton: UIView!
    
    var activityIndicator : FeedAppActivityIndicator!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let size = CGSize(width: 100, height: 100)
        
        
        activityIndicator = FeedAppActivityIndicator(bounds: CGRect(x: (view.frame.width / 2) - size.width / 2, y: (view.frame.height / 2) - size.height / 2, width: size.width, height: size.height), color: UIColor.lightGrayColor())
        view.layer.addSublayer(activityIndicator)
        
        
        
        // Do any additional setup after loading the view.
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        activityIndicator.startAnimation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func dismissAction(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }
    
}
