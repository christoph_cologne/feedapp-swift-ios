//
//  ProductsToDetailsTransition.swift
//  FeedApp
//
//  Created by Christoph Bohnen on 15.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit


class ProductsToDetailsTransition:NSObject, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate{
    
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        let fromVC = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as! ProductsViewController
        let toVC = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) as! DetailViewController
        
        let containerView = transitionContext.containerView()!
        let duration = self.transitionDuration(transitionContext)
        
        // Snapshot
        
        let animatedCell = fromVC.collectionView.cellForItemAtIndexPath((fromVC.collectionView.indexPathsForSelectedItems()!.first )! ) as! ProductsCollectionViewCell
        let snapshot = animatedCell.imageView.snapshotViewAfterScreenUpdates(false)
        snapshot.frame = containerView.convertRect(animatedCell.imageView.frame, fromView: animatedCell.imageView.superview)
        
        
        
        // init states
        toVC.view.frame = transitionContext.finalFrameForViewController(toVC)
        toVC.view.alpha = 0
        (toVC.pageViewController.viewControllers![0] as! DetailImagePageViewController).productImageView.hidden = true
        
        containerView.addSubview(toVC.view)
        containerView.addSubview(snapshot)
        
        UIView.animateWithDuration(duration, animations: { () -> Void in
            toVC.view.alpha = 1
            snapshot.frame =
                CGRect(x:   containerView.convertRect(toVC.pageViewController.view.frame, fromView: toVC.pageViewController.view).origin.x, y:   containerView.convertRect(toVC.pageViewController.view.frame, fromView: toVC.pageViewController.view).origin.y, width: toVC.view.frame.width, height:   containerView.convertRect(toVC.pageViewController.view.frame, fromView: toVC.pageViewController.view).height)
            
            
            }) { (finished) -> Void in
                (toVC.pageViewController.viewControllers![0] as! DetailImagePageViewController).productImageView.hidden = false
                animatedCell.hidden = false
                snapshot.removeFromSuperview()
                
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
                
        }

    }
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.2
    }
    
    
}