//
//  ProductAdapter.swift
//  feedApp
//
//  Created by Christoph Bohnen on 25.03.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import Foundation

class ProductAdapter {
 
    var productsList :[Product] = []
    struct ProductKeys {
        static let nameKey = "name"
        static let priceKey = "price"
        static let imageURLKey = "img"
        static let idKey = "id"
    }
    
    
    
    func adaptDataToProduct(callback:([Product])->Void, networkFailure:()->Void){
 
        self.productsList = []
        
        NetworkManager.sharedInstance.requestProducts ({ (JSON) -> Void in
            
            
            for product in JSON {
              
                let id = (product["\(ProductKeys.idKey)"] as? String)
                let name = (product["\(ProductKeys.nameKey)"] as? String)
                let imgURLS = (product["\(ProductKeys.imageURLKey)"] as? [String])
                let price = (product["\(ProductKeys.priceKey)"] as? Double)
                
                let productInstance = Product(id:id,name: name,imageURLS: imgURLS,price: price)
                self.productsList.append(productInstance)
                }
 
             callback(self.productsList)
        },
            networkFailureCallback: {
                networkFailure()
            }
        )
    }
    
    
   
    
    
}