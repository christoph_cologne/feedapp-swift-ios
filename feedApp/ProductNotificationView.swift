//
//  ProductNotificationView.swift
//  ProductNotificationView
//
//  Created by Christoph Bohnen on 21.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit

class ProductNotificationView: UIView {
    
    @IBOutlet var baseInformationLabel: UILabel!    
    @IBOutlet var additionalInformationLabel: UILabel!
    let animationDuration:NSTimeInterval =  3
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)

    }
    override func layoutSubviews() {
        super.layoutSubviews()
        baseInformationLabel.textColor = UIColor.whiteColor()
        additionalInformationLabel.textColor = UIColor.whiteColor()
    }
    
    
    
    func displayToggle(title:String,detail:String){

        
        let labelTitle = String(format: NSLocalizedString("PRODUCT ADDED NOTIFICATION TITLE" ,  comment: "shows up as title when product is added to list"), title )
        
        let labelDetailInfo = String(format: NSLocalizedString("PRODUCT ADDED NOTIFICATION DETAIL" ,  comment: "shows up as detailinfo when product is added to list"), detail )
        
        
        baseInformationLabel.text = labelTitle
        additionalInformationLabel.text = labelDetailInfo
        
        hidden = false
        
        UIView.animateWithDuration(self.animationDuration, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: { () -> Void in
            self.alpha = 1
       
            
        }) { (finished) -> Void in
            if(finished){
                UIView.animateWithDuration(self.animationDuration, delay: 0, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
                    self.alpha = 0
                }, completion: { (finished) -> Void in
                    if(finished){
                        self.hidden = true
                    }
                })
            }
            
        }
        
//        
//        baseInformationLabel.text = ""
//        additionalInformationLabel.text = ""
    }
    
    
    
}
