//
//  AboutPaginationViewController.swift
//  FeedApp
//
//  Created by Christoph Bohnen on 04.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit


class AboutPaginationViewController: UIViewController,UIPageViewControllerDataSource {

    class FeedAppAboutInformation {
        let title :String
        let backgroundImage:UIImage
        init(title:String,backgroundImage:UIImage){
            self.title = title
            self.backgroundImage = backgroundImage
        }
        
        
        
    }
    
    let pageInformations:[FeedAppAboutInformation] =
    [
        FeedAppAboutInformation(title: NSLocalizedString("PURPOSE OF THE APPLICATION", comment: "the purpose of the application"), backgroundImage: UIImage(named: "about1.jpg")!),
        FeedAppAboutInformation(title: NSLocalizedString("DETAILS ABOUT APPLICATION", comment: "the details about the application"), backgroundImage: UIImage(named: "about2.jpg")!),
        FeedAppAboutInformation(title:  NSLocalizedString("LIBRARIES USED", comment: "used libraries"), backgroundImage: UIImage(named: "about3.jpg")!)
    
    ]
    

    
    let pageviewController = UIPageViewController(transitionStyle: UIPageViewControllerTransitionStyle.Scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.Horizontal, options: [UIPageViewControllerOptionInterPageSpacingKey:0])
    
    
    let toolbarHeight : CGFloat = 44

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageviewController.dataSource = self
        
      
        pageviewController.view.frame = CGRect(x: 0, y: toolbarHeight, width: view.frame.width, height: view.frame.height - toolbarHeight)
        
       
        
        let dismissButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: "dismissVC:")
        

        pageviewController.view.backgroundColor = UIColor.lightGrayColor()
        
       
        
        self.addChildViewController(pageviewController)
        view.addSubview(pageviewController.view)

        
        

    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
         pageviewController.setViewControllers([setupViewControllerBy(0)], direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: nil)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func setupViewControllerBy(index:Int)->AboutPageViewController{

        return AboutPageViewController(pageIndex: index, pageTitle: pageInformations[index].title, image: pageInformations[index].backgroundImage)
        
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?{
        
        let vc = viewController as! AboutPageViewController
        var index = vc.pageIndex as Int
     
        if(index == 0 || index == NSNotFound){
            return nil
        }
        
        
        index--
        
        return setupViewControllerBy(index)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController?{
        let vc = viewController as! AboutPageViewController
        var index = vc.pageIndex as Int
       
        
        if(index == NSNotFound || index == pageInformations.count - 1){
            return nil
        }
        
        index++
        
        return setupViewControllerBy(index)
    }
    
    
    
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int{
     
        return pageInformations.count
    }

    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int{
        return 0
    }
    

    func dismissVC(sender:AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    @IBAction func dismissAction(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
