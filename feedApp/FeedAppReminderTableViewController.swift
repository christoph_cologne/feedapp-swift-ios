//
//  FeedAppReminderTableViewController.swift
//  FeedApp
//
//  Created by Christoph Bohnen on 22.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit

class FeedAppReminderTableViewController: UITableViewController {

    @IBOutlet var navigationBar: UINavigationBar!
    @IBOutlet var resignButton: UIBarButtonItem!
    var remindedProducts: [Product]!
    
    @IBOutlet var editButton: UIBarButtonItem!
    
    let sharedDefaults = NSUserDefaults(suiteName: "group.christophbohnen.FeedApp")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let productsData = sharedDefaults?.objectForKey("products") as! NSData
        remindedProducts = NSKeyedUnarchiver.unarchiveObjectWithData(productsData) as! [Product]
        
        tableView.reloadData()
    
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        return remindedProducts.count
    }

    
    @IBAction func dismissAction(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("remindedCell", forIndexPath: indexPath) as! FeedAppReminderTableViewCell

        let image =  remindedProducts[indexPath.row].getImages()[0] as UIImage
        cell.productNameLabel.text = remindedProducts[indexPath.row].getName()
        cell.productImageView.image = image

        return cell
    }
    

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }


    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            
            remindedProducts.removeAtIndex(indexPath.row)
            
            let productsData = NSKeyedArchiver.archivedDataWithRootObject(remindedProducts)
            sharedDefaults?.setObject(productsData, forKey: "products")
            sharedDefaults?.synchronize()

            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
    }
    
    
    @IBAction func didTouchEdit(sender: UIBarButtonItem) {
        if(tableView.editing == false){
            tableView.setEditing(true, animated: true)
            
        }
        else{
            tableView.setEditing(false, animated: true)
        }
        
    }

}
