//
//  ProductsViewController.swift
//  feedApp
//
//  Created by Christoph Bohnen on 24.03.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit
import SWReveal

protocol ProductsViewControllerDelegate{
    func segueToDetails(indexPath:NSIndexPath)
}


class ProductsViewController: UIViewController,SWRevealViewControllerDelegate,ProductsViewControllerDelegate {
    
  
    @IBOutlet var optionsBarButton: UIBarButtonItem!
    
    //   MARK: revealViewController
    var swrevealView :SWRevealViewController!
    
    
    
    var activityIndicator : FeedAppActivityIndicator!
    
    
    //   MARK: blurView properties
    lazy var blurEffect:UIBlurEffect = {
        return UIBlurEffect(style: UIBlurEffectStyle.Dark)
        }()
    lazy var blurView : UIVisualEffectView = {
        return UIVisualEffectView(effect: self.blurEffect)
        }()
    
    lazy var vibrancyEffect : UIVibrancyEffect = {
        return UIVibrancyEffect(forBlurEffect: self.blurEffect)
        }()
    
    lazy var vibrancyView : UIVisualEffectView = {
        return UIVisualEffectView(effect: self.vibrancyEffect)
        }()
    
    
    
    let productApdaper = ProductAdapter()
    var modalView :ModalPresentView!
    var alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.Alert)
    
    var isGrid = true
    
    var chosenCell :UICollectionViewCell?
    
    
    @IBOutlet var collectionView: ProductsCollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buildRevealView()
        requestProducts()
        setUpAlertView()
        
        
        activityIndicator = FeedAppActivityIndicator(bounds: CGRect(x: (view.frame.width / 2) - 25, y: (view.frame.height / 2) - 25, width: 50, height: 50), color: UIColor.lightGrayColor())
        view.layer.addSublayer(activityIndicator)
        activityIndicator.startAnimation()
        
        
        collectionView.refreshControl.addTarget(self, action: "didPullToRefresh:", forControlEvents: UIControlEvents.ValueChanged)
        
        collectionView.productsViewControllerDelegate = self
        
        collectionView.gridLayout = GridViewFlowLayout(size: view.bounds.size)
        collectionView.listLayout = ListViewFlowLayout(size: view.bounds.size)
        

        collectionView.setCollectionViewLayout(collectionView.gridLayout, animated: true)
        

        
        
        
    }
    
    // MARK: Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        switch segue.identifier! {
        case "ShowProductDetail":
            
            let indexPath = sender as! NSIndexPath
            if let dvc = segue.destinationViewController as? DetailViewController{
                
                dvc.product = self.collectionView.products[indexPath.row]
                dvc.firstProductImage = (self.collectionView.cellForItemAtIndexPath(indexPath) as! ProductsCollectionViewCell).imageView.image
                dvc.cellIndexPath = indexPath
                
                
            }
            
        default:
            break
            
        }

    }
    
    // MARK: alertView
    func setUpAlertView(){
        
        alertController.title = "No Connection"
        alertController.message = "Unfortunately you can not establish a Connection to our servers"
        alertController.addAction(UIAlertAction(title: "reload", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            
            switch action.style{
            case .Default:
                print("reload")
                self.requestProducts()
            default:
                break
            }
            
            
        }))
        
    }
    
    func requestProducts(){
        
        
        self.productApdaper.adaptDataToProduct({ (products) -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.collectionView.products = products
                self.collectionView.reloadData()
                self.activityIndicator.stopAnimation()
            })
            },
            networkFailure: {
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in })
        })
        
        
    }
    
    

    override func didReceiveMemoryWarning() {
        print("memory warning!")
    }
    
    // MARK: grid/list Button action -- Change grid to list and vice-versa
    @IBAction func gridList(sender: UIBarButtonItem) {
        
        if(collectionView.products.count > 0 ){
            
            self.collectionView.listLayout.referenceSize = self.view.bounds.size
            self.collectionView.gridLayout.referenceSize = self.view.bounds.size
            
            let layoutCompletionHandler = {(finished:Bool) -> Void in
                self.collectionView.reloadItemsAtIndexPaths(self.collectionView.indexPathsForVisibleItems())
            }
            
            
            
            // TODO change constraints instead of reload
            if(isGrid){
                
                collectionView.cellIdentifier = "ProductCellList"
                collectionView.setCollectionViewLayout(collectionView.listLayout, animated: true, completion: layoutCompletionHandler)
                sender.title = "Grid"
                isGrid = false
                
                
            }
            else{
                collectionView.cellIdentifier = "ProductCell"
                collectionView.setCollectionViewLayout(collectionView.gridLayout, animated: true, completion: layoutCompletionHandler)
                sender.title = "List"
                isGrid = true
                
                
            }
            
            
            collectionView.collectionViewLayout.invalidateLayout()
            collectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.None, animated: true)
            
            
        }
    }
    
    
    //  MARK: SWReveal Delegate Methods and associated functions
    
    func buildRevealView(){
        swrevealView = self.revealViewController()
        swrevealView.delegate = self
        
        blurView.frame = self.view.bounds
        blurView.alpha = 0
        
        optionsBarButton.target = swrevealView
        optionsBarButton.action = Selector("revealToggle:")

        
        
        
        
        view.addGestureRecognizer(swrevealView.panGestureRecognizer())
    }
    
    
    func revealController(revealController: SWRevealViewController!, panGestureMovedToLocation location: CGFloat, progress: CGFloat, overProgress: CGFloat) {
        if(progress > 0 && progress < 1){
            self.navigationController?.view.addSubview(blurView)
            blurView.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            blurView.alpha = progress
        }
        
    }
    
    
    func animateBlurView(open:Bool){
        
        // check if blurView is already a subview of self.view
        
        if(!self.blurView.isDescendantOfView((self.navigationController?.view)!)){
            self.navigationController?.view.addSubview(self.blurView)
        }
        
        let alpha:CGFloat
        alpha = open ? 1 : 0
        
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            
            self.blurView.alpha = alpha
            
            }) { (finished) -> Void in
                
                if (finished) {
                    var gestureView:UIView
                    
                    if open {
                        self.blurView.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
                        gestureView = self.blurView
                        
                    }
                    else{gestureView = self.view}
                    
                    gestureView.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                }
                
        }
    }
    
    
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        (self.collectionView.collectionViewLayout as! FeedAppCollectionViewFlowLayout).referenceSize = size
        
        self.collectionView.collectionViewLayout.invalidateLayout()
        
        
        
    }
    
    
    func revealController(revealController: SWRevealViewController!, animateToPosition position: FrontViewPosition) {
        
        animateBlurView(position == FrontViewPosition.Right)
        
    }
    
    
    func segueToDetails(indexPath:NSIndexPath) {
        
        self.chosenCell = self.collectionView.cellForItemAtIndexPath(indexPath)


        
         performSegueWithIdentifier("ShowProductDetail", sender: indexPath)

        
    }
    
    func didPullToRefresh(sender:AnyObject){
        requestProducts()
        collectionView.refreshControl.endRefreshing()
        activityIndicator.startAnimation()
    }
    
    
    
}
