//
//  FeedAppProgressViewController.swift
//  FeedApp
//
//  Created by Christoph Bohnen on 22.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit

class FeedAppProgressViewController: UIViewController {

    @IBOutlet var backButton: UIBarButtonItem!
    @IBOutlet var backgroundView: UIView!
    
    var progressView : ProgressView!
    var progress = CGFloat(0)
    
    var timer : NSTimer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        progressView = ProgressView(frame: CGRect(origin: CGPoint(x: 0, y: 90), size: view.frame.size), backgroundView: backgroundView, withModal: false)
        view.addSubview(progressView)
        progressView.progressCircle.strokeColor = UIColor.blackColor().CGColor
        progressView.progressLabel.textColor = UIColor.blackColor()
        progressView.activate()

        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        progress = 0
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "incrementProgress", userInfo: nil, repeats: true)
        
    
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    

        
     
    }
    
    func incrementProgress(){
        if(progress < 1){
            progress = progress + 0.1
            progressView.animateToProgess(progress)
        }
        else{
            timer.invalidate()
        }
        
    }
        
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissAction(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }
    
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
