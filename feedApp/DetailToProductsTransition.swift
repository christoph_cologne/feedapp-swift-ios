//
//  DetailToProductsTransition.swift
//  FeedApp
//
//  Created by Christoph Bohnen on 15.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit

class DetailToProductsTransition:NSObject, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate{
    
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        let fromVC = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as! DetailViewController
        let toVC = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) as! ProductsViewController
        
        
        let containerView = transitionContext.containerView()
        let duration = self.transitionDuration(transitionContext)

    
        
        let snapshot = (fromVC.pageViewController.viewControllers![0] as! DetailImagePageViewController).productImageView.snapshotViewAfterScreenUpdates(false)
        snapshot.clipsToBounds = true
        snapshot.contentMode =  UIViewContentMode.ScaleAspectFill
        
        
        snapshot.frame = containerView!.convertRect((fromVC.pageViewController.viewControllers!.last as! DetailImagePageViewController).productImageView.frame, fromView: (fromVC.pageViewController.viewControllers!.last as! DetailImagePageViewController).productImageView)
        
        (fromVC.pageViewController.viewControllers!.last as! DetailImagePageViewController).productImageView.hidden = true
        
        let animatedCell = toVC.collectionView.cellForItemAtIndexPath(fromVC.cellIndexPath) as! ProductsCollectionViewCell
        animatedCell.imageView.hidden = true
        
        toVC.view.frame = transitionContext.finalFrameForViewController(toVC)
        containerView!.insertSubview(toVC.view, belowSubview: fromVC.view)
        containerView!.addSubview(snapshot)
        
        
        
        UIView.animateWithDuration(duration, animations: { () -> Void in
            fromVC.view.alpha = 0
            snapshot.frame = containerView!.convertRect(animatedCell.imageView.frame, fromView: animatedCell.imageView.superview)
    
            
        }) { (finished) -> Void in
            snapshot.removeFromSuperview()
           (fromVC.pageViewController.viewControllers!.last as! DetailImagePageViewController).productImageView.hidden = false
            animatedCell.imageView.hidden = false
        
                        
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
        
            
        }
        
    }
    
    
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.2
    }
}
