//
//  ImageViewController.swift
//  feedApp
//
//  Created by Christoph Bohnen on 01.04.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//



// TODO make it a mere view of the detailVC

import UIKit

class ImageViewController: UIViewController,UIScrollViewDelegate {

    var image:UIImage?
    
    var productName:String? {
        willSet{
            self.navigationItem.title = newValue
        }
    }

    @IBOutlet var imageScrollView: UIScrollView!

    
    lazy var imageView :UIImageView! = {
        let imageView = UIImageView()
        imageView.image = self.image
        imageView.contentMode = UIViewContentMode.Center
        return imageView
        
    }()
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.sizeToFit()
        imageScrollView.contentSize = imageView.frame.size
        imageScrollView.addSubview(imageView)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
