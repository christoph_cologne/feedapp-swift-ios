//
//  FeedAppMapResultsTableViewController.swift
//  FeedApp
//
//  Created by Christoph Bohnen on 22.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit
import MapKit



//UITextFieldDelegate,FeedAppMapViewProtocol for MapViewController


protocol MapResultDelegateProtocol{
func updateTableView()
var hasResults:Bool{get set}
func resignResults()
}

protocol MapResultDataSoureProtocol{
func setMapResults(results: [FeedAppMapQueryResult])
}
protocol FeedAppMapViewProtocol{
func setDestination(destinationIndex:Int)

}




class FeedAppCLPlacemark: CLPlacemark {
    lazy var adress:String = {
        if(self.thoroughfare != nil ){
            return "\(self.thoroughfare), \(self.locality)"
        }
        else{
//            return "\(self.locality)"
            return "is nil"
        }

    }()
}

class FeedAppMapQueryResult{
    
    var locality:String?
    var thoroughfare:String?
    var subThoroughfare:String?
    var coordinates:CLLocationCoordinate2D?
    init(locality:String,thoroughfare:String,subThoroughfare:String,coordinates:CLLocationCoordinate2D){
        self.locality = locality
        self.thoroughfare = thoroughfare
        self.subThoroughfare = subThoroughfare
        self.coordinates = coordinates
    }
    
    lazy var adress:String = {
        if(self.thoroughfare != nil){
            return "\(self.thoroughfare), \(self.locality)"
        }
        else{
            return self.locality!
        }
    }()
    
}


class FeedAppMapResultsTableViewController: UITableViewController ,MapResultDelegateProtocol,MapResultDataSoureProtocol {
    
    var results :[FeedAppMapQueryResult] = []
    
    var mapViewDelegate : FeedAppMapViewProtocol!
    
    
    var hasResults = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layer.cornerRadius = 5
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return results.count
    }
    
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCellWithIdentifier("resultCell")! 
        
//        cell.textLabel?.text = results[indexPath.row]?.adress ?? "is nil"
        return cell
        
    }
    
    func updateTableView() {
        self.tableView.reloadData()
        print("results are : \(self.results)")
    }
    
    func setMapResults(results: [FeedAppMapQueryResult]) {
        self.results = results
        self.tableView.reloadData()
    }
    
    func resignResults() {
        performSegueWithIdentifier("dismissMapResult", sender: self)
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        if(results[indexPath.row]!.subThoroughfare != ""){
//           println("ask for number")
//        }
//        else{
//            mapViewDelegate.setDestination(indexPath.row)
//        }
//        
//        
    }
    
    
 
    
}
