//
//  NetworkManager.swift
//  feedApp
//
//  Created by Christoph Bohnen on 25.03.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import Foundation
import UIKit
class NetworkManager:NSObject{

    let session = NetworkSession.getDefaultSession()
    let debugMode = true

    func requestProducts(requestCallback:([NSDictionary])->Void,networkFailureCallback:()->Void ){
        
        
       let task = session.dataTaskWithURL(NSURL(string: APPLICATION_URL.MAIN_URL)!, completionHandler: { (data, response, error) -> Void in
        
            if let httpResponse = response as? NSHTTPURLResponse{
        
       
                print("error is : \(error) code is : \(httpResponse.statusCode)")
                if (httpResponse.statusCode == 200){
                    JSONParser.parseJSON(data!, callback: { (dic) -> Void in
                        requestCallback(dic)
                    })
                }
                else{
                    // MARK: handle networkfailure
             
                    //                 println(error.code)
                    if(self.debugMode){
                        
                         self.getProductsFromJSONFile({ (dic) -> Void in requestCallback(dic) })
                                            
                    }
                    else{
                        networkFailureCallback()
                    }
                    
                }
                
            }
            else{
                if(self.debugMode){
                    
                    self.getProductsFromJSONFile({ (dic) -> Void in requestCallback(dic) })
                }
                else{
                    networkFailureCallback()
                }

            }
        
        })


        
        task.resume()
        
    }
    
    
    func imgFromServer(urls:[String],callback:(AnyObject)-> Void){
        
        if (!debugMode){
            for url in urls{
                
                let task = session.dataTaskWithURL(NSURL(string: APPLICATION_URL.IMAGE_URL + url)!, completionHandler: { (data, response, error) -> Void in
                    
                    
                    callback(data!)
                })
                task.resume()
            }
        }
        else{
            getImagesLocally(urls, callback: { (imgData) -> Void in
                callback(imgData)
            })
        }
    
    }

    
    
    func getDetailInformation(id:String,callback:([String:String])->Void){
        if(!debugMode){
       
            let request = NSMutableURLRequest()
            request.URL = NSURL(string:APPLICATION_URL.DESCR_URL)
            request.HTTPMethod = "GET"
            request.setValue(id, forHTTPHeaderField: "id")
            
            
           let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
                JSONParser.parseJSON(data!, callback: { (dic) -> Void in
                    let descr = dic[0]["descr"] as! String
                    callback(["descr":descr])
                })
            })
            task.resume()
            
        }
        else{
            
            let path = NSBundle.mainBundle().pathForResource("LoremIpsum", ofType: "txt")
            let content = (try? String(contentsOfFile: path!, encoding: NSUTF8StringEncoding)) ?? "no content found"
            callback(["descr":content])
        }
        
    }

    //  MARK: JSONParser
    
    
    class JSONParser{
        static func parseJSON(data:NSData,callback:([NSDictionary])->Void){
            print("loaded from jsonfile")
            
            do {

                if let json = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as? [NSDictionary]  {
                    
                    
                    if (data.length != 0) {
                        callback(json)
                    }
                    else{
                        print("error: data length is 0!")
                    }
                    
                }
                else{
                    print("json doesnt exist \nprobably invalid JSON ")
                }
            }
            catch let e {
                print(e)
            }
            
        }
    }
    
    
    static let sharedInstance = NetworkManager()
    
    
    
    // MARK: debug Methods
    
    
    private func getProductsFromJSONFile(callback:([NSDictionary])->Void){
        let path = NSBundle.mainBundle().pathForResource("products", ofType: "json")
        let data = try? NSData(contentsOfFile: path!, options: .DataReadingMappedIfSafe)
        JSONParser.parseJSON(data!, callback: { (dic) -> Void in
            callback(dic)
        })
        
    }
    
    private func getImagesLocally(urls:[String],callback:(AnyObject)-> Void){
        for url in urls{
            let imagePath = NSBundle.mainBundle().pathForResource(url, ofType: "jpg")
            let imageData = NSData(contentsOfFile: imagePath!)
            callback(imageData!)
            
        }
    }
    


    class NetworkSession{
        var defaultSession:NSURLSession!
        init(){
            let sessionConfig = NSURLSessionConfiguration.defaultSessionConfiguration()
            sessionConfig.timeoutIntervalForRequest = 2 //MARK: timeOut -- set higher in PROD
            sessionConfig.allowsCellularAccess = true
            self.defaultSession = NSURLSession(configuration: sessionConfig)
            
        }
        static func getDefaultSession()->NSURLSession{
            let session = NetworkSession()
            return session.defaultSession
        }
    }

    

}



class OperationQueues{
    
    var downloadQueue : NSOperationQueue!
    init(){
      downloadQueue = NSOperationQueue()
      downloadQueue.name = "Download Queue"
      downloadQueue.qualityOfService = NSQualityOfService.Background
        
    }
    
    static let sharedInstance = OperationQueues()
}




