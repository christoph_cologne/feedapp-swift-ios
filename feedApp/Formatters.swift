//
//  Formatters.swift
//  FeedApp
//
//  Created by Christoph Bohnen on 06.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import Foundation

class Formatter{
    
    let numberFormatter = NumberFormatter()
    let dateFormatter = DateFormatter()
    let currencyFormatter = CurrencyFormatter()
    let percentFormatter = PercentageFormatter()
    
    static let sharedInstance = Formatter()
}


class DateFormatter:NSDateFormatter {
    override init() {
        super.init()
        self.locale = NSLocale.currentLocale()
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}

class NumberFormatter:NSNumberFormatter {
    
    override init(){
        super.init()
        self.numberStyle = NSNumberFormatterStyle.DecimalStyle
        self.locale = NSLocale.currentLocale()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    

}

class CurrencyFormatter: NSNumberFormatter {
    
    override init(){
        super.init()
        self.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        self.locale = NSLocale.currentLocale()

        self.maximumFractionDigits = 2
        self.minimumFractionDigits = 2
    }
    

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
  
}

class PercentageFormatter:NSNumberFormatter {
    
    override init(){
        super.init()
        self.numberStyle = NSNumberFormatterStyle.PercentStyle
        self.locale = NSLocale.currentLocale()

            
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}



