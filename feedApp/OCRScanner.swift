//
//  OCRScanner.swift
//  FeedApp
//
//  Created by Christoph Bohnen on 02.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import Foundation
import AVFoundation
import Tesseract


class OCRScanner:AVManager,G8TesseractDelegate{
    
    var stillImage : AVCaptureStillImageOutput!
    var scanRect  :CGRect!
    
    var progressIndicator:ProgressView!
    
    override init(frame: CGRect, parentViewController: UIViewController, shallStartOnInit: Bool) {
        
        super.init(frame: frame, parentViewController: parentViewController, shallStartOnInit: shallStartOnInit)
        
        progressIndicator = ProgressView(frame: frame, backgroundView: previewView, withModal: true)
        
        stillImage = AVCaptureStillImageOutput()
        stillImage.outputSettings = [AVVideoCodecKey:AVVideoCodecJPEG,AVVideoQualityKey:0.6]
        
        if(self.captureSession.canAddOutput(self.stillImage)){
            self.captureSession.addOutput(self.stillImage)
        }
        
        let padding : CGFloat = 44
        let scanRectWidth = self.previewView.frame.width - padding
        
        self.scanRect = CGRect(x: padding / 2, y: (self.previewView.frame.height / 2) - (scanRectWidth / 2) - 50, width: scanRectWidth, height: scanRectWidth)
        
        let scanRectPath = UIBezierPath(rect:scanRect)
        
        let scanRectShape = CAShapeLayer()
        scanRectShape.path = scanRectPath.CGPath
        scanRectShape.fillRule = kCAFillRuleEvenOdd
        scanRectShape.fillColor = UIColor.clearColor().CGColor
        scanRectShape.strokeColor = UIColor.blackColor().CGColor
        
        self.previewLayer.addSublayer(scanRectShape)
        
        previewView.addSubview(progressIndicator)
        progressIndicator.backgroundColor = UIColor(red: 0.3, green: 0.3, blue: 0.3, alpha: 0.85)
        
    }
    
    
    //   MARK: - OCR core
    
    func takePicture(){
        
        progressIndicator.activate()
        
        (parentViewController as! TesseractOCRViewController).tessarctDidStartScanning()
        
        dispatch_async(SECOND_QUEUE, { () -> Void in
            let videoCon = self.stillImage.connectionWithMediaType(AVMediaTypeVideo)
            
            self.stillImage.captureStillImageAsynchronouslyFromConnection(videoCon, completionHandler: { (buf:CMSampleBuffer!, er:NSError!) -> Void in
                
                let data = AVCaptureStillImageOutput
                    .jpegStillImageNSDataRepresentation(buf)
                let image = UIImage(data:data)
                
                
                self.performImageRecognitionAsync(image!)
                
            })
            
        })
        
    }
    
    
    func performImageRecognitionAsync(image:UIImage){
        
        self.stopRunning()
        
        let startTime = CACurrentMediaTime()
        let operation = G8RecognitionOperation(language: "eng+deu")
        
        operation.tesseract.image = self.scaleImage(image, maxDimension: 640)
        
        
        operation.progressCallbackBlock =  {(tesseract)  in
            
            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                self.progressIndicator.animateToProgess((CGFloat(tesseract.progress) / (CGFloat(100))))
            })
        }
        
        
        
        operation.recognitionCompleteBlock = {(tesseract) in
            
            let endTime = CACurrentMediaTime()
            self.progressIndicator.deActivate()
            (self.parentViewController as! TesseractOCRViewController).tesseractDidFinishScanning(tesseract.recognizedText, duration: endTime - startTime )
            
        }
        
        
        
        operation.queuePriority = NSOperationQueuePriority.VeryHigh
        operation.qualityOfService = NSQualityOfService.UserInitiated
        let operationQueue = TesseractQueue()
        operationQueue.addOperation(operation)
        
        
        

        
        
        
    }
    
    
    
    
    private func scaleImage(image:UIImage,maxDimension:CGFloat)->UIImage{
        let startTime = CACurrentMediaTime()
        
        var scaledSize = CGSizeMake(maxDimension, maxDimension)
        var scaleFactor:CGFloat
        
        if image.size.width > image.size.height {
            scaleFactor = image.size.height / image.size.width
            scaledSize.width = maxDimension
            scaledSize.height = scaledSize.width * scaleFactor
        } else {
            scaleFactor = image.size.width / image.size.height
            scaledSize.height = maxDimension
            scaledSize.width = scaledSize.height * scaleFactor
        }
        
        
        
        UIGraphicsBeginImageContext(scaledSize)
        image.drawInRect(CGRectMake(0, 0, scaledSize.width, scaledSize.height))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let endTime = CACurrentMediaTime()
        
        print("Time scaling - \(endTime - startTime)")
        
        return scaledImage
    }
    
    class TesseractQueue: NSOperationQueue {
        override init() {
            super.init()
            qualityOfService = NSQualityOfService.UserInitiated
//            maxConcurrentOperationCount = 1
            name = "tesseractQueue"
            
        }
    }
    
    
}