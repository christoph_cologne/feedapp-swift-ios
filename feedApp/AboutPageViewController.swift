//
//  AboutPageViewController.swift
//  FeedApp
//
//  Created by Christoph Bohnen on 04.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit

class AboutPageViewController: UIViewController {

    @IBOutlet var labelCenterXConstraint: NSLayoutConstraint!
    @IBOutlet var baseView: UIView!

    @IBOutlet var label:UILabel!

    @IBOutlet var backgroundImageView: UIImageView!
    
    var pageIndex :Int!
    var pageTitle: String!
    var image:UIImage!
    
        
    init(pageIndex:Int,pageTitle:String,image:UIImage){
        super.init(nibName: "AboutView", bundle: nil)
        self.pageIndex = pageIndex
        self.pageTitle = pageTitle
        self.image = image
        
    }

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label.text = pageTitle
        
        self.backgroundImageView.image = image
        
        
        self.labelCenterXConstraint.constant = (self.view.frame.width / 2) * -1
        self.baseView.layoutIfNeeded()
        self.labelCenterXConstraint.constant = 0
        
        
        
        UIView.animateWithDuration(2, delay: 0, usingSpringWithDamping: 2, initialSpringVelocity: 1, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
            

            self.baseView.layoutIfNeeded()
            
        }, completion: nil)

    
        


    }
    

 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
