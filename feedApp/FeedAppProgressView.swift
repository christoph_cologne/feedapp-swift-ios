//
//  ProgressView.swift
//  CAShapeLayerDemo
//
//  Created by Christoph Bohnen on 17.06.15.
//  Copyright (c) 2015 Guanshan Liu. All rights reserved.
//

import UIKit

class ProgressView: UIView {

    let progressCircle : ProgessCirlce!
    let circleAnimation = CircleProgressAnimation()
    let progressLabel :ProgressLabel!
    let backgroundView : UIView!
    let withModal:Bool!
    
    
    init(frame:CGRect,backgroundView:UIView,withModal:Bool){
        let padding:CGFloat = 50
        self.backgroundView = backgroundView
        self.progressCircle = ProgessCirlce(rect: frame,centerPoint: CGPoint(x: frame.width / 2, y: frame.height / 2))
        
        
        
        self.progressLabel = ProgressLabel(frame: CGRect(x: 0 + (padding / 2), y: 0 + (padding / 2), width: frame.width - padding, height: frame.height - padding))
        
        self.withModal = withModal
        
        super.init(frame: frame)
        
        circleAnimation.delegate = progressCircle
        
        backgroundColor = UIColor.clearColor()

        if(withModal == true){
                backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
            
        }
        else{
            backgroundColor = UIColor.clearColor()
        }
        
        hidden = true
        alpha = 0
    }
    


    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
    
        layer.addSublayer(progressCircle)
        addSubview(progressLabel)
        
        
    }
    func activate(){
        hidden = false
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            
              self.alpha = 1
        })
    }
    func deActivate(){
        
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            self.alpha = 0
        }) { (finished) -> Void in
            self.hidden = true
        }
    }
    
    
    func animateToProgess(progress:CGFloat){
        
        circleAnimation.fromValue = circleAnimation.toValue
        circleAnimation.toValue = progress
        progressCircle.addAnimation(circleAnimation, forKey: "strokeEnd")
        
        progressCircle.currentValue = progress
        progressCircle.strokeEnd = progress
        progressLabel.text = "\(Int(progress * 100))"
        
    }
    
    
    class ProgessCirlce: CAShapeLayer {
        
        var currentValue :CGFloat!
        
        let circleWidth : CGFloat = 100
        
        override init(layer: AnyObject) {
            super.init(layer: layer)
        }
        
        override init(){
            super.init()
        }
        
        
        init(rect:CGRect,centerPoint:CGPoint){
            super.init()
            bounds = CGRect(origin: centerPoint, size: CGSize(width: circleWidth, height: circleWidth))
            let radius: CGFloat = max(bounds.width, bounds.height) / 2
            position = centerPoint
            
            let toValue = CGFloat( 2 * M_PI)
            
            
            
            let arcCenter = CGPoint(x: (rect.width / 2) + (circleWidth / 2) , y: (rect.height / 2) + (circleWidth / 2))
            fillColor = UIColor.clearColor().CGColor
            fillMode = kCAFillRuleEvenOdd
            
            let arcWidth:CGFloat = 1
            
            path = UIBezierPath(arcCenter: arcCenter, radius: radius - arcWidth / 2, startAngle: 0, endAngle: toValue, clockwise: true).CGPath
            
            miterLimit = 1
            strokeEnd = 0
            strokeColor = UIColor.whiteColor().CGColor
            
            
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        override func animationDidStop(anim: CAAnimation, finished flag: Bool) {
//            super.animationDidStop(anim, finished: flag)
            
        }
        
    }
    
    
    class CircleProgressAnimation: CABasicAnimation{
        override init() {
            super.init()
            keyPath = "strokeEnd"
            repeatCount = 0
            timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    class ProgressLabel:UILabel{
        override init(frame: CGRect) {
            super.init(frame: frame)

            font = UIFont(name: "HelveticaNeue-Light", size: 29)
            textAlignment = NSTextAlignment.Center
            text = "0"
            textColor = UIColor.whiteColor()
        }

        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        
    }
    

    
    
}

