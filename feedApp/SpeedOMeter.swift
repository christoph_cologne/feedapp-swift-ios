//
//  SpeedOMeter.swift
//  Speedometer
//
//  Created by Christoph Bohnen on 20.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit

class SpeedOMeter : UIView{
 
    
    var backgroundLayer:BackgroundLayer!
    var pointerLayer:Pointer!
    let pointerRotation:PointerRotation!
    
    
    
    var displayLabel :UILabel!
    override init(frame: CGRect) {
        backgroundLayer = BackgroundLayer(frame: CGRect(origin: CGPointZero, size: frame.size))
        pointerLayer = Pointer(frame: CGRect(origin: CGPointZero, size: frame.size),bg:backgroundLayer)
        pointerRotation = PointerRotation(keyPath: "transform.rotation")
        
        displayLabel = UILabel(frame: CGRect(x: 0, y: frame.height - 50, width: frame.width, height: 50))
        
        
        super.init(frame: frame)
        
        backgroundLayer.insertSublayer(pointerLayer, atIndex: 0)
        layer.addSublayer(backgroundLayer)
    
        displayLabel.textAlignment = NSTextAlignment.Center
        self.addSubview(displayLabel)
        

        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        pointerRotation = PointerRotation()
        super.init(coder: aDecoder)
    }

    
    class BackgroundLayer :CAShapeLayer {
        
        let _fillColor = UIColor.whiteColor().CGColor
        let _strokeColor = UIColor.blackColor().CGColor
        let _lineWidth = CGFloat(2)
        
        let endAngle = CGFloat( 3 * M_PI ) / CGFloat(4)
        let startAngle = CGFloat(M_PI) / CGFloat(4)
        
        init(frame:CGRect){
            super.init()
            
            bounds = frame
            self.frame = frame
            
            
            let backgroundPath =  UIBezierPath(arcCenter: CGPoint(x: frame.width / 2, y: frame.height / 2), radius: frame.width / 2, startAngle: startAngle, endAngle: endAngle, clockwise: false)
            
            
            path = backgroundPath.CGPath
            fillColor = _fillColor
            strokeColor = _strokeColor
            lineWidth = _lineWidth
            
        }
        
        override init(){
            super.init()
            
            
        }
        override init(layer: AnyObject) {
            super.init(layer: layer)
        }
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        
    }
    
    
    class Pointer:CAShapeLayer {
        let _strokeColor = UIColor.blackColor().CGColor
        let _lineWidth = CGFloat(3)
        var bg:BackgroundLayer!
        
        init(frame:CGRect,bg:BackgroundLayer){
            super.init()
            
            self.bg = bg
            self.frame = frame
            self.bounds = frame
            
            anchorPoint = CGPoint(x: 0.5, y: 0.5)
            
            let pointerPath = UIBezierPath()
            let otherPath = UIBezierPath(CGPath: self.bg.path!)
            
            pointerPath.moveToPoint(CGPathGetCurrentPoint(otherPath.CGPath))
            pointerPath.addLineToPoint(CGPoint(x: bounds.width / 2, y: bounds.height / 2))
            
            path = pointerPath.CGPath
            strokeColor = _strokeColor
            strokeStart = 0.1
            strokeEnd = 0.8
            lineWidth = _lineWidth
            
        }
        
        override init() {
            super.init()
        }
        override init(layer: AnyObject) {
            super.init(layer: layer)
        }
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    }
    

    class PointerRotation:CABasicAnimation {
        override init() {
            super.init()
            removedOnCompletion = false
            fillMode = kCAFillModeForwards
            duration = 0
            
            
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    }
    
    func moveToValue(value:CGFloat){
        
        let toValue = value * CGFloat( M_PI + (M_PI / 2))

            pointerRotation.toValue = toValue
            pointerRotation.fromValue = toValue
            CATransaction.begin()

            pointerLayer.addAnimation(pointerRotation, forKey: "transform.rotation")
            CATransaction.commit()
        
    
    }
    
    
}


