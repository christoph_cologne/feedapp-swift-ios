//
//  ProductsNavigationViewController.swift
//  CollectionViewTransitionTest
//
//  Created by Christoph Bohnen on 15.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit

class ProductsNavigationViewController: UINavigationController,UINavigationControllerDelegate {
    
    var leftEdgePanGestureRecognizer :UIScreenEdgePanGestureRecognizer!
    
    var interActivePopTransition :UIPercentDrivenInteractiveTransition!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        self.navigationBar.titleTextAttributes = [NSFontAttributeName:UIFont(name: "Lakesight Personal Use Only", size: 22)!]
        
        self.navigationBar.barStyle = UIBarStyle.BlackTranslucent
        
        leftEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: "didSwipeLeftEdge:")
        leftEdgePanGestureRecognizer.edges = UIRectEdge.Left
        self.view.addGestureRecognizer(leftEdgePanGestureRecognizer)
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
    
        
        // check if segueing from ProductsViewController
        if(fromVC.isKindOfClass(ProductsViewController) || fromVC.isKindOfClass(DetailViewController) && toVC.isKindOfClass(DetailViewController) || toVC.isKindOfClass(ProductsViewController)){
            
            switch operation {
            case .Push:
                return ProductsToDetailsTransition()
            case .Pop:
                return DetailToProductsTransition()
            default:
                return nil
                
            }
            
        }
        
        
        return nil
        
    }
    
    func navigationController(navigationController: UINavigationController, interactionControllerForAnimationController animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        if (animationController.isKindOfClass(DetailToProductsTransition)){
            return self.interActivePopTransition
        }
        else{
            return nil
        }
    }
    
    func didSwipeLeftEdge(sender:UIScreenEdgePanGestureRecognizer){
        var progress:CGFloat = (sender.translationInView(self.view).x / (self.view.bounds.size.width * 1))
        
    
        
        progress = min(1.0, max(0.0,progress))
        
        if (sender.state == UIGestureRecognizerState.Began){
      
            
            self.interActivePopTransition = UIPercentDrivenInteractiveTransition()
            self.popViewControllerAnimated(true)
        }
        else if (sender.state == UIGestureRecognizerState.Changed){
            self.interActivePopTransition.updateInteractiveTransition(progress)
        }
        else if (sender.state == UIGestureRecognizerState.Ended || sender.state == UIGestureRecognizerState.Cancelled){
            if(progress > 0.5){
                self.interActivePopTransition.finishInteractiveTransition()
            }
            else{
                self.interActivePopTransition.cancelInteractiveTransition()
            }
            
            self.interActivePopTransition = nil
            
        }
        
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
