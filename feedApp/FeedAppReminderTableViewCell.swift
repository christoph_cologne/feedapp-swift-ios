//
//  FeedAppReminderTableViewCell.swift
//  FeedApp
//
//  Created by Christoph Bohnen on 23.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit

class FeedAppReminderTableViewCell: UITableViewCell {

    @IBOutlet var productImageView: UIImageView!

    @IBOutlet var productNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
