//
//  AVManager.swift
//  feedApp
//
//  Created by Christoph Bohnen on 02.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit
import AVFoundation

protocol AVManagerDelegate{
//    func didStartRunning()
//    func didStopRunning()
    func CapturingDeviceUsesAutoFocus()
}



class AVManager:NSObject {
 
    var captureSession :AVCaptureSession!
    var captureDevice :AVCaptureDevice!
    var output :AVCaptureOutput!
    var videoInput : AVCaptureDeviceInput!
    var previewView:UIView!
    var previewLayer :AVCaptureVideoPreviewLayer!
    var parentViewController:UIViewController!
    var isRunning = false
    var delegate:AVManagerDelegate!
    
    init(frame:CGRect,parentViewController:UIViewController,shallStartOnInit:Bool) {
        super.init()
        

        self.parentViewController = parentViewController
        
        captureSession = AVCaptureSession()
        if let captureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo) {
            self.captureDevice = captureDevice
            var videoInputErr :NSError?
            do {
                self.videoInput = try AVCaptureDeviceInput(device: self.captureDevice)
            } catch let error as NSError {
                videoInputErr = error
                self.videoInput = nil
            }
            
            if(self.captureSession.canAddInput(self.videoInput)){
                self.captureSession.addInput(self.videoInput)
            }
            
            self.captureDevice.addObserver(self, forKeyPath: "adjustingFocus", options: [], context: nil)
        }

        self.previewView = UIView(frame: frame)
        self.parentViewController.view.addSubview(previewView)


        
        self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
        self.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        
            
        previewLayer.frame = frame
        previewLayer.bounds = parentViewController.view.bounds
    
        
        previewView.bounds = parentViewController.view.bounds
     

        self.previewView.layer.addSublayer(self.previewLayer)
        captureSession.startRunning()
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "enterForeground", name:UIApplicationWillEnterForegroundNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "enterBackground", name:UIApplicationDidEnterBackgroundNotification, object: nil)
        
        
        
        if(shallStartOnInit){
            isRunning = true
            startRunning()
        }
        
        

     }
    
    func startRunning(){
        if(!isRunning){
            captureSession.startRunning()
            isRunning = true
        }
    }
    
    func stopRunning(){
        if(isRunning){
            captureSession.stopRunning()
            isRunning = false
        }
    }
    
    
    func enterForeground(){
        self.startRunning()
        print("ENTERED FOREGROUND")
    }
    
    func enterBackground(){
        self.stopRunning()
        print("ENTERED BACKGROUND")
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        
        if(keyPath == "adjustingFocus"){
            print("focussing")
            //      TODO animating!      delegate.CapturingDeviceUsesAutoFocus()
            
        }
        
    }
    


}