//
//  Constants.swift
//  FeedApp
//
//  Created by Christoph Bohnen on 26.05.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import Foundation
import UIKit

let MAINBUNDLE = NSBundle.mainBundle()
let MAINSTORYBOARD = UIStoryboard(name: "Main", bundle: nil)
let SECOND_QUEUE = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)

struct APPLICATION_URL {
    static let BASE_URL = MAINBUNDLE.objectForInfoDictionaryKey("BaseURL") as! String
    static let MAIN_URL = BASE_URL + (MAINBUNDLE.objectForInfoDictionaryKey("SubURLS") as! [String:String])["MainURL"]! as String
    static let IMAGE_URL = BASE_URL + (MAINBUNDLE.objectForInfoDictionaryKey("SubURLS") as! [String:String])["ImageURL"]! as String
    static let DESCR_URL = BASE_URL + (MAINBUNDLE.objectForInfoDictionaryKey("SubURLS") as! [String:String])["DescriptionURL"]! as String
    
}
