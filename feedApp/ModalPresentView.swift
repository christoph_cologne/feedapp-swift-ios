//
//  ModalPresentView.swift
//  feedApp
//
//  Created by Christoph Bohnen on 09.04.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit

class ModalPresentView: UIView {

    var baseView:UIView!
    var blurView:UIVisualEffectView!{
        let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.ExtraLight))
    
        return blurEffectView
    }
    
    let notificationLabel = UILabel()
    
    var notificationLabelLeadingSpace : NSLayoutConstraint!
    var notificationLabelTrailingSpace : NSLayoutConstraint!
    var notificationLabelBottomSpace : NSLayoutConstraint!
    var notificationLabelTopSpace : NSLayoutConstraint!
    
    
    init(frame:CGRect,baseView:UIView,notification:String){
        super.init(frame:baseView.frame)
        self.baseView = baseView
        self.backgroundColor = UIColor.whiteColor()
        self.frame = frame
        blurView.frame = bounds
        self.addSubview(blurView)
        
      
        self.alpha = 0
        applyBorder()
        applyShadow()
        
        baseView.addSubview(self)
        
        setUpLabel(notification)
      
        self.addSubview(notificationLabel)
        setUpConstraints()
        
        
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
 
    }
    
    func applyShadow(){
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOffset = CGSize(width: 10, height: 5)
        layer.shadowOpacity = 0.2
        layer.shadowRadius = 5
    }
    
    func applyBorder(){
        layer.cornerRadius = 10
        layer.borderColor = UIColor.darkGrayColor().CGColor
        layer.borderWidth = 1
    }
    func setUpLabel(notification:String){
        notificationLabel.frame = CGRect(x: 0 , y: 0, width: self.frame.width , height: self.frame.height)
        notificationLabel.numberOfLines = -1
        notificationLabel.textAlignment = NSTextAlignment.Center
        notificationLabel.textColor = UIColor.darkTextColor()
        notificationLabel.text = notification
//        notificationLabel.adjustsFontSizeToFitWidth = true
        
    }
    
    func setUpConstraints(){

        notificationLabel.translatesAutoresizingMaskIntoConstraints = false


        notificationLabelLeadingSpace = NSLayoutConstraint(item: notificationLabel, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Leading, multiplier: 1, constant: 10)
                
        notificationLabelTopSpace = NSLayoutConstraint(item: notificationLabel, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 15)
        
        notificationLabelTrailingSpace = NSLayoutConstraint(item: notificationLabel, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Trailing, multiplier: 1, constant: -15)
        
        notificationLabelBottomSpace = NSLayoutConstraint(item: notificationLabel, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: -10)
        
     
        
        
      self.addConstraints([notificationLabelTopSpace,notificationLabelBottomSpace,notificationLabelLeadingSpace,notificationLabelTrailingSpace])
        
    }
    
    func dispaySelfAsNotifaction(){

        UIView.animateWithDuration(1, delay: 0, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
                self.alpha = 1
        }) { (finished) -> Void in
            
            let delay = 1.0 * Double(NSEC_PER_SEC)
            let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
            dispatch_after(time, dispatch_get_main_queue(), {
                
                UIView.animateWithDuration(1, animations: { () -> Void in
                    self.alpha = 0
                })

            })
        }
        
    }
    
    
}
