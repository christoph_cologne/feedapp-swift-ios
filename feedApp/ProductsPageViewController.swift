//
//  ProductsPageViewController.swift
//  FeedApp
//
//  Created by Christoph Bohnen on 16.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit

protocol FeedAppPageViewControllerProtocol{
    var product:Product {get set}
}

class ProductsPageViewController: UIPageViewController,UIPageViewControllerDataSource {

    var product:Product!
    
    var initialLoad = true

    override init(transitionStyle style: UIPageViewControllerTransitionStyle, navigationOrientation: UIPageViewControllerNavigationOrientation, options: [String : AnyObject]?) {
        super.init(transitionStyle: style, navigationOrientation: navigationOrientation, options: options)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        
            
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: pageView indicators
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return (product?.getImgCount())!
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int{
        return 0
    }

    //    MARK: pageViewController delegate methods
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?{
        
        
        let vc = viewController as! DetailImagePageViewController
        if(vc.pageIndex == 0 || vc.pageIndex == NSNotFound){
            return nil
        }
        else{
            return setupPageViewController(vc.pageIndex, isAfterVC: false)
        }
        
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController?{
        
        let vc = viewController as! DetailImagePageViewController
        if(vc.pageIndex == ((product?.getImgCount())! - 1) || vc.pageIndex == NSNotFound){
            return nil
        }
        else{
            var x =  { (viewController) -> UIViewController in
                
                return viewController
            }
            
            return setupPageViewController(vc.pageIndex, isAfterVC: true)
            
            
        }
        
    }
    
    
    
    
    
    func setupPageViewController(index:Int,isAfterVC:Bool?)->UIViewController{
        
        var detailVC :DetailImagePageViewController!
        self.createPageViewByLoadingFromServer(isAfterVC,index,&detailVC,&self.product! )
        return detailVC
        
    }
    
    var createPageViewByLoadingFromServer = {(operation:Bool?,index:Int,inout detail:DetailImagePageViewController!, inout product:Product)-> Void in
        
        var currentIndex = index
        if(operation != nil){
            operation! ? currentIndex++ :currentIndex--
        }
        
        product.getImage([product.getImgURL(currentIndex)],callback: {
            (image,urls) -> Void in
            detail = DetailImagePageViewController(pageIndex: currentIndex,image: image, imageName: product.getImgURL(currentIndex))
            
            var currentImages = product.getImages() as [UIImage]!
            currentImages.append(image)
            
            product.setImages(currentImages)
        })
    }
    
    
    var createPageViewByLoadingFromMemory = {(inout detailVC:DetailImagePageViewController, inout product:Product,index:Int) -> Void in
        
        
        detailVC = DetailImagePageViewController(pageIndex: index, image: (product.getImages()[index]), imageName: (product.getImgURL(index))!)
    }

    
}
