//
//  Product.swift
//  feedApp
//
//  Created by Christoph Bohnen on 25.03.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import Foundation
import UIKit

@objc(Product)
class Product :NSObject, NSCoding{
    
    private var id:String?
    private var imageURLS:[String]!
    private var name:String?
    private var images:[UIImage]?
    
    private var price:Double?
    func priceAsStringWithCurrency()->String!{
        if (self.price != nil){
            return Formatter.sharedInstance.currencyFormatter.stringFromNumber(self.price!)
        }
        else{
             // i18n
             return NSLocalizedString("NO PRICE AVAILABLE", comment: "Explain user that there is no price available for this product due to possible error")
        }
    }
    
    private lazy var imageURL:String = {
        let mainBundle = NSBundle.mainBundle()
        
        let baseURL :String = mainBundle.objectForInfoDictionaryKey("BaseURL") as! String
        let subURLS = (mainBundle.objectForInfoDictionaryKey("SubURLS") as! [String:String])
        let imageURL = baseURL + (subURLS["ImageURL"] as String?)!
        
        return baseURL
        
    }()
    
    func completeImagePath(position:Int)->String{
    
            return imageURL + getImgURL(position) + ".jpg"
    }


    
    override init(){
        
    }
    
    init(id:String?,name:String?,imageURLS:[String]?,price:Double?){

        self.id = id
        self.name = name
        self.imageURLS = imageURLS
        self.price = price
        
    }
    
    func getId()->String?{
        return id
    }
    
    func setId(id:String){
        self.id = id
    }

        //      unwrapped  to handle unset value with i18n
    
    func getName()->String! {
        return name ?? NSLocalizedString("NO NAME AVAILABLE",comment:"to display when there is no name available")
    }
    func setName(name :String){
        self.name = name
    }
    
    func getImgURL(position:Int)->String!{
        return imageURLS[position] ?? NSLocalizedString("NO IMAGE AVAILABLE",comment:"to display when there is no image available")
    }
    
    func getFirstImgURL()->String!{
        return getImgURL(0)
    }
    
    func getImgURL()->[String]?{
        return imageURLS
    }
    
    func getImgCount()->Int{
        return imageURLS.count ?? 0
    }
    
    
    func getImage(urls:[String],callback:(UIImage,[String])->Void){
        NetworkManager.sharedInstance.imgFromServer(urls, callback: { (data) -> Void in

            let image = UIImage(data: data as! NSData)!
  
            callback(image ,urls )
        })
    }
    
    func setImgURL(position:Int,newURL:String){
        self.imageURLS[position] = newURL
    }
    
    func getImages()->[UIImage]{
        return self.images ?? []
    }
    
    func setImages(images:[UIImage]){
        self.images = images
    }
    
    
    
    //    MARK:  - NSCoding
    
    
    func encodeWithCoder(aCoder: NSCoder){

        aCoder.encodeObject(self.price, forKey: "price")
        aCoder.encodeObject(self.name, forKey: "name")
        aCoder.encodeObject(self.id, forKey: "id")
        let encodedImageArray = [self.getImages()[0]]
        aCoder.encodeObject(encodedImageArray, forKey: "image")
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init()
        
        
        id = aDecoder.decodeObjectForKey("id") as? String
        price = aDecoder.decodeObjectForKey("price") as? Double
        name = aDecoder.decodeObjectForKey("name") as? String
        images = aDecoder.decodeObjectForKey("image") as? [UIImage]
        
        
    }
    
    
    
}