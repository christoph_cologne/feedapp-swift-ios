//
//  SidebarTableViewController.swift
//  feedApp
//
//  Created by Christoph Bohnen on 19.04.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit


class SidebarTableViewController: UITableViewController {
    
    class FeedAppSidebarElement {
        let title:String
        let detail:String
        init(title:String,detail:String){
            self.title = title
            self.detail = detail
        }
    }
    
    
    @IBOutlet var headerLabel: UILabel!
    
    
    // i18n !
    
    let sideBarElements : [FeedAppSidebarElement] = [
        
        FeedAppSidebarElement(title:  NSLocalizedString("HOW LONG DO I NEED", comment: "ask the question how long I need to drive to the company"), detail: NSLocalizedString("HOW LONG DO I NEED - DETAIL",  comment: "detail for mapView in Sidebar")),
        FeedAppSidebarElement(title:
            NSLocalizedString("REMINDED PRODUCTS", comment: "button title for reminded products tableView"), detail: NSLocalizedString("THE REMINDED LIST", comment: "the title for the reminded list")),
        FeedAppSidebarElement(title: NSLocalizedString("BARCODE, QR-CODE SCANNER", comment:"explain that here can be a scanner"), detail: NSLocalizedString("BARCODE SCANNER DETAILS", comment: "the details for the barcode scanner title")),
        FeedAppSidebarElement(title: NSLocalizedString("OCR SCANNER",comment: "OCR scanner"), detail: "Tesseract Library"),
        FeedAppSidebarElement(title: "Spinner Experiment", detail: "UIBezierPath Spinner Animation"),
        FeedAppSidebarElement(title: "Progress Experiment", detail: "UIBezierPath Progress View"),
        FeedAppSidebarElement(title: "Slider Experiment", detail: "UIBezierPath Arc"),
        FeedAppSidebarElement(title: NSLocalizedString("ABOUT US", comment: "information about the application"), detail: NSLocalizedString("ABOUT US - DETAIL", comment: "detail information about the application"))
        
        
    ]
    
    lazy var notificationSwitch : UISwitch = {
        let notificationSwitch = UISwitch(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        notificationSwitch.addTarget(self, action: "notifactionSettingChanged:", forControlEvents: UIControlEvents.ValueChanged)
        
        return notificationSwitch
        }()
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        notificationSwitch.on = UIApplication.sharedApplication().isRegisteredForRemoteNotifications()
        (self.tableView as! SidebarTableView).revealWidth = self.revealViewController().rightViewRevealOverdraw
        
        headerLabel.frame = CGRect(x: headerLabel.frame.origin.x, y: headerLabel.frame.origin.y, width:  CGRectGetWidth(view.frame), height: headerLabel.frame.height)
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return sideBarElements.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SidebarCell", forIndexPath: indexPath) 
        
        
        
        cell.textLabel?.text = sideBarElements[indexPath.row].title
        cell.detailTextLabel?.text = sideBarElements[indexPath.row].detail
        
        return cell
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        if(indexPath.row == 0){
            self.performSegueWithIdentifier("showMap", sender: self)
            return
        }
        
        if(indexPath.row == 1){
            self.performSegueWithIdentifier("showReminded", sender: self)
            return
        }
        
        
        if(indexPath.row == 2){
            self.performSegueWithIdentifier("showBarcodeScanner", sender: self)
            return
            
        }
        if(indexPath.row == 3){
            self.performSegueWithIdentifier("showOCRScanner", sender: self)
            return
        }
        
        if(indexPath.row == 4){
            self.performSegueWithIdentifier("showSpinner", sender: self)
            return
        }
        if(indexPath.row == 5){
            self.performSegueWithIdentifier("showProgress", sender: self)
            return
        }
        
        if(indexPath.row == 6){
            self.performSegueWithIdentifier("showSlider", sender: self)
            return
        }
        
        if(indexPath.row == 7){
            self.performSegueWithIdentifier("showAbout", sender: self)
            return
        }
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        if(segue.identifier == "showBarcodeScanner"){
            //             containerDelegate?.addVC(segue.destinationViewController as! BarCodeViewController)
            if let dvc = segue.destinationViewController as? BarcodeViewController{
                
            }
        }
        
        
    }
    
    
}
