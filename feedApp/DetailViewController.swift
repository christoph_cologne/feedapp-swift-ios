//
//  DetailViewController.swift
//  feedApp
//
//  Created by Christoph Bohnen on 26.03.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit
import SWReveal

class DetailViewController: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet var baseScrollView: UIScrollView!
    
    @IBOutlet var productTitleLabel: UILabel!
    @IBOutlet var bigButton: UIButton!
    @IBOutlet var productNameLabel: UILabel!
    @IBOutlet var productSmallDescriptionLabel: UILabel!
    @IBOutlet var productPriceLabel: UILabel!
    @IBOutlet var productBigDescriptionLabel: UILabel!
    
    @IBOutlet var detailBackground: UIView!
    
    @IBOutlet var pageViewBackgroundView: UIView!
    
    var firstProductImage:UIImage?
    
    var pageViewController = ProductsPageViewController(transitionStyle: UIPageViewControllerTransitionStyle.Scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.Horizontal, options: nil)
    
    
    // MARK: the data!
    var product:Product?
    
    
    var cellIndexPath : NSIndexPath!
    
    
    var notificationView:ProductNotificationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        notificationView = NSBundle.mainBundle().loadNibNamed("NotificationView", owner: self, options: nil)[0] as! ProductNotificationView
        
        notificationView.frame = detailBackground.bounds
        detailBackground.addSubview(notificationView)
        
        notificationView.baseInformationLabel.text = "title"
        notificationView.additionalInformationLabel.text = "text"
        
        notificationView.hidden = true
        notificationView.alpha = 0
        
        pageViewController.view.frame = pageViewBackgroundView.bounds
        pageViewBackgroundView.addSubview(pageViewController.view)
        view.bringSubviewToFront(pageViewController.view)
        pageViewController.product = self.product
        
        setUpContent()
        
        bigButton.layer.cornerRadius = 5
        bigButton.addTarget(self, action: "selectedProduct:", forControlEvents: UIControlEvents.TouchUpInside)
        bigButton.setTitle( NSLocalizedString("DETAILVIEW BUTTON", comment: "bigbutton in detailView")
, forState: UIControlState.Normal)
        
        
        view.bringSubviewToFront(detailBackground)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    private func setUpContent(){
        productTitleLabel.text = product?.getName()
        productNameLabel.text = product?.getName()
        productSmallDescriptionLabel.text = product?.getImgURL(0)
        productPriceLabel.text = product?.priceAsStringWithCurrency()
        navigationItem.title = product?.getName()
        
        
        NetworkManager.sharedInstance.getDetailInformation((product?.getId())!, callback: { (dic) -> Void in
            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                
                self.productBigDescriptionLabel.text = dic["descr"]
            })
            
        })
        
        
        pageViewController.setViewControllers([pageViewController.setupPageViewController(0, isAfterVC: nil)], direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: {
            (finished) -> Void in
//             if (self.initialLoad){self.initialLoad = false}
        })
        
    }
    
    
    //   MARK: Segues
    
    func showImageInFullSize(sender:UITapGestureRecognizer){
        
        performSegueWithIdentifier("showFullScreenImage", sender: sender)
        
        
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "showFullScreenImage"){
            let dvc = segue.destinationViewController as! ImageViewController
            
            
            dvc.image = (sender?.view as! UIImageView).image
            dvc.productName = product?.getName()
            
        }
    }
    

    

    
    func selectedProduct(sender:UIButton){
     
        let sharedDefaults = NSUserDefaults(suiteName: "group.christophbohnen.FeedApp")
        var unarchivedProducts:[Product]
        
        if let currentProductsData = sharedDefaults?.objectForKey("products") as? NSData{
            
            unarchivedProducts = NSKeyedUnarchiver.unarchiveObjectWithData(currentProductsData) as! [Product]
        }
        else{
             unarchivedProducts = [product!]
        }
 
        

        
        var productsIDArray : [String] = []
        for product in unarchivedProducts{
            productsIDArray.append(product.getId()!)
        }


        
        if(!productsIDArray.contains(((product?.getId())!))){
            unarchivedProducts.append(product!)
        }
        
        let productsData = NSKeyedArchiver.archivedDataWithRootObject(unarchivedProducts)
        sharedDefaults?.setObject(productsData, forKey: "products")
        sharedDefaults?.synchronize()
        
        
        
        notificationView.displayToggle((product?.getName())!, detail: (product?.getName())!)
        
        
        let localNotification = UILocalNotification()
        localNotification.fireDate = NSDate(timeIntervalSinceNow: 2)
        localNotification.alertBody = "\((product?.getName())!)"
        localNotification.alertAction = "\((product?.getName())!)"
        localNotification.timeZone = NSTimeZone.defaultTimeZone()
        localNotification.applicationIconBadgeNumber = 1
        
    
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
        
        
        
        
        
        
    }
    
    
    
}