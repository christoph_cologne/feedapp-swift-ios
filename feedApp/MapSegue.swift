//
//  MapSegue.swift
//  mapview
//
//  Created by Christoph Bohnen on 11.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit

class MapSegue: UIStoryboardSegue {
 
    override func perform() {
        let destinationVC = self.destinationViewController as! FeedAppMapResultsTableViewController
        let sourceVC = self.sourceViewController as! FeedAppMapViewController
        
        let sourceFrame = sourceVC.view.bounds        
        
//        let destinationHeight = sourceFrame.height - sourceVC.keyboardSize.height - (sourceVC.navigationController?.navigationBar.frame.maxY)!
        let destinationHeight = sourceFrame.height - (sourceVC.navigationController?.navigationBar.frame.maxY)!
        
        let destinationSidePadding:CGFloat = 25
        
        let destinationFrame = CGRect(x: sourceFrame.origin.x + destinationSidePadding, y: sourceFrame.height, width: sourceFrame.width - destinationSidePadding * 2 , height: destinationHeight)
        
        destinationVC.view.frame = destinationFrame
        destinationVC.view.alpha = 0
        
        sourceVC.addChildViewController(destinationVC)
        sourceVC.view.addSubview(destinationVC.view)
        
        
        
        UIView.animateWithDuration(0.7, delay: 0, usingSpringWithDamping: 2, initialSpringVelocity: 2, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
            
            destinationVC.view.frame = CGRect(x: destinationVC.view.frame.origin.x, y:(sourceVC.navigationController?.navigationBar.frame.maxY)!, width: destinationVC.view.frame.width, height: destinationHeight )
            
            destinationVC.view.alpha = 1
        }) { (finished) -> Void in
            
        }
        
        // set delegate
//        sourceVC.mapResultDelegate = destinationVC
//        sourceVC.mapResultDatasource = destinationVC
//        destinationVC.mapViewDelegate = sourceVC
        
    }
    
        
    
    
}
