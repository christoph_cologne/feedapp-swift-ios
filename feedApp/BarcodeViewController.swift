//
//  BarcodeViewController.swift
//  FeedApp
//
//  Created by Christoph Bohnen on 31.05.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit
import AVFoundation

class BarcodeViewController: UIViewController {

    var barcodeScanner : BarcodeScanner!
    @IBOutlet var dismissButton: UIButton!
    var buttonGreenColor:UIColor!
    
    @IBOutlet var infoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        buttonGreenColor = dismissButton.backgroundColor
        barcodeScanner = BarcodeScanner(frame: view.frame, parentViewController: self)
        
        dismissButton.layer.cornerRadius = 5
        dismissButton.addTarget(self, action: "dismissVC:", forControlEvents: UIControlEvents.TouchUpInside)
        
        
        
        view.bringSubviewToFront(dismissButton)
        view.bringSubviewToFront(infoLabel)
    }
    

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissVC(sender:AnyObject){
        barcodeScanner.stopRunning()
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


}
