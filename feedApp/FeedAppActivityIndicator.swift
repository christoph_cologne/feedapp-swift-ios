//
//  FeedAppActivityIndicator.swift
//  FeedApp
//
//  Created by Christoph Bohnen on 17.06.15.
//
//

import UIKit

class FeedAppActivityIndicator: CAShapeLayer {
    let rotateAnimation:RotationAnimation!
    
    init(bounds:CGRect,color:UIColor){
        rotateAnimation = RotationAnimation()
        super.init()
        
        position = CGPoint(x: CGRectGetMidX(bounds), y: CGRectGetMidY(bounds))
        fillRule = kCAFillRuleEvenOdd
        fillColor = UIColor.clearColor().CGColor
        let center = CGPoint(x:bounds.width/2, y: bounds.height/2)
        let radius: CGFloat = max(bounds.width, bounds.height) / 2
        

        let arcWidth: CGFloat =  1.5
       
        let startAngle: CGFloat = CGFloat(11 * M_PI) / 18
        let endAngle: CGFloat =  CGFloat(M_PI / 4)
        

        path = UIBezierPath(arcCenter: center,
            radius: radius - arcWidth / 2,
            startAngle: startAngle,
            endAngle: endAngle,
            clockwise: true).CGPath


        lineWidth = arcWidth

        strokeColor = color.CGColor
        hidden = true
        
        self.bounds = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func startAnimation(){
        hidden = false
        self.addAnimation(self.rotateAnimation, forKey: "transform.rotation")
    }
    func stopAnimation(){
        hidden = true
        self.removeAnimationForKey("transform.rotation")
    }
    
}

class RotationAnimation: CABasicAnimation {
    override init() {
        super.init()
        keyPath = "transform.rotation"
        cumulative = true
        fromValue = CGFloat(0)
        toValue = CGFloat(M_PI * 2.0)
        duration = 1
        repeatCount = Float.infinity
    }
 
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
}


