//
//  TesseractOCRViewController.swift
//  FeedApp
//
//  Created by Christoph Bohnen on 02.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit

class TesseractOCRViewController: UIViewController {
    
    @IBOutlet var dismissButton: UIButton!
    
    @IBOutlet var trigger: UIButton!
    
    var tesseractOpticalCharacterScanner :OCRScanner!
    var responseView: FeedAppTesseractResponseView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tesseractOpticalCharacterScanner = OCRScanner(frame: view.frame, parentViewController: self, shallStartOnInit: true)
        
        dismissButton.layer.cornerRadius = 5
        dismissButton.addTarget(self, action: "dismissVC:", forControlEvents: UIControlEvents.TouchUpInside)
        
        
        view.sendSubviewToBack(tesseractOpticalCharacterScanner.previewView)
        
        trigger.addTarget(tesseractOpticalCharacterScanner, action: "takePicture", forControlEvents: UIControlEvents.TouchUpInside)
        
        trigger.layer.cornerRadius = trigger.frame.width / 2
        
        responseView = FeedAppTesseractResponseView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height),tesseractObject:self.tesseractOpticalCharacterScanner)
        
        
        
        view.addSubview(responseView)
        
        
    }
    // MARK: - tesseract delegate
    // tesseract delegate
    func tessarctDidStartScanning(){
        
    }
    
    
    func tesseractDidFinishScanning(recognizedText:String,duration:CFTimeInterval){
        print("recognized: \(recognizedText), duration: \(duration))")
        
        responseView.showViewWithText(recognizedText)
        
    }
    
    
    func dismissVC(sender: UIButton) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }
    
    
    
    class FeedAppTesseractResponseView:UIView,UITextViewDelegate {
        
        let resignButtonHeight = CGFloat(50)
        var textView:TesseractResultTextView!
        var resignButton:UIButton!
        let animationDuration = 0.3
        let tesseractObject:OCRScanner!
        
        
        
        
        init(frame: CGRect,tesseractObject:OCRScanner) {
            self.tesseractObject = tesseractObject
            super.init(frame: frame)
            
            
            self.textView = TesseractResultTextView(frame: CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.width, height: frame.height - resignButtonHeight))
            
            self.addSubview(self.textView)
            
            self.textView.delegate = self
            
            self.resignButton = ResponseViewResignButton(frame: CGRect(x: frame.origin.x
                , y: self.textView.frame.height, width: frame.width, height: resignButtonHeight))
            self.resignButton.addTarget(self, action: "hideView", forControlEvents: UIControlEvents.TouchUpInside)
            
            self.addSubview(self.resignButton)
            hidden = true
            alpha = 0
        }
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func showViewWithText(text:String?){
            hidden = false
            if let recognizedText = text {
                self.textView.text = recognizedText
            }
            
            UIView.animateWithDuration(animationDuration, animations: { () -> Void in
                self.alpha = 1
            })
        }
        func hideView(){
            UIView.animateWithDuration(animationDuration, animations: { () -> Void in
                self.alpha = 0
                }) { (finished) -> Void in
                    if(finished){
                        self.hidden = true
                        self.tesseractObject.startRunning()
                    }
            }
            
        }
        
        func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
            if(text == "\n"){
                textView.resignFirstResponder()
                return false
            }
            return true
        }
        
        
        
        class TesseractResultTextView: UITextView,UITextViewDelegate {
            
            
            override init(frame: CGRect, textContainer: NSTextContainer?) {
                super.init(frame: frame, textContainer: textContainer)
                tintColor = UIColor.grayColor()
                textColor = UIColor.grayColor()
                textAlignment = NSTextAlignment.Left
                font = UIFont.systemFontOfSize(16)
                
            }
            required init?(coder aDecoder: NSCoder) {
                fatalError("init(coder:) has not been implemented")
            }
        }
        
        class ResponseViewResignButton:UIButton {
            let bgColor = UIColor.blackColor()
            let fontColor = UIColor.whiteColor()
            let title = NSLocalizedString("DISMISS", comment: "dismiss title for buttons")
            
            override init(frame: CGRect) {
                super.init(frame: frame)
                self.backgroundColor = bgColor
                self.titleLabel?.textColor = fontColor
                self.setTitle(title, forState: UIControlState.Normal)
            }
            
            required init?(coder aDecoder: NSCoder) {
                fatalError("init(coder:) has not been implemented")
            }
        }
        
        
        
    }
    
    
    
    
    
    
}
