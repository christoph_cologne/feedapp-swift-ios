//
//  SidebarTableView.swift
//  FeedApp
//
//  Created by Christoph Bohnen on 31.05.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit

class SidebarTableView: UITableView {

    var revealWidth: CGFloat!
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
        self.frame = CGRect(x: 0, y: 0, width: rect.width - revealWidth, height: rect.height)
    }
    


}
