//
//  AppDelegate.swift
//  feedApp
//
//  Created by Christoph Bohnen on 24.03.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit
import SWReveal


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        //        Override point for customization after application launch.
        
        //MARK: local notifications
        if let locationNotification = launchOptions?[UIApplicationLaunchOptionsLocalNotificationKey] as? UILocalNotification{
            
            application.applicationIconBadgeNumber = 0
            application.cancelAllLocalNotifications()
        }
        
        
        // MARK: remote notfications
        let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
        
        
        // MARK: set appereances
        
        let pageControl = UIPageControl.appearance()
        pageControl.pageIndicatorTintColor = UIColor.lightGrayColor()
        pageControl.currentPageIndicatorTintColor = UIColor.blackColor()
        pageControl.backgroundColor = UIColor.whiteColor()
        
        return true
    }

    
    
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        
        application.applicationIconBadgeNumber = 0
        application.cancelAllLocalNotifications()
        
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        
      
        print("Got token data! \(deviceToken)")
    
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print("Couldn’t register deviceToken for Push Notification: \(error)")
    }
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        // inspect notificationSettings to see what the user said!
    }
   
    
    func showAlert(){
        
    }


    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
        if(application.applicationState == UIApplicationState.Active){
            
            let localNotification = UILocalNotification()
            localNotification.fireDate = NSDate(timeIntervalSinceNow: 1)
            localNotification.alertBody = "Info"
            localNotification.alertAction = "Info"
            localNotification.timeZone = NSTimeZone.defaultTimeZone()
            localNotification.applicationIconBadgeNumber = 1
            
            application.scheduleLocalNotification(localNotification)
            
        }
        
    }
    
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {

        
//        let rootViewController = self.window?.rootViewController as! SWRevealViewController
//        let navigationController = rootViewController.frontViewController as! UINavigationController
//        let detailViewController = navigationController.topViewController as! DetailViewController
        
        
    }

    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        
        print("annotation:\(annotation)")
        print("url:\(url)")
        
        print("pathExtension\(url.pathExtension)")
        print("pathComponents: \(url.pathComponents)")
        print("path: \(url.path)")
        print("pathHost: \(url.host)")
        print("query: \(url.query)")
        
        if(url.host == "product" && url.query != ""){
            
            
        }
        
        
        return true
        
    }
    

}

