//
//  ProductsCollectionViewCell.swift
//  feedApp
//
//  Created by Christoph Bohnen on 24.03.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit

class ProductsCollectionViewCell: UICollectionViewCell {
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var bottomBackgroundView: UIView!
    @IBOutlet var productName: UILabel!
    @IBOutlet var productDescription: UILabel!
    @IBOutlet var productPrice: UILabel!

    
    @IBOutlet var imageViewBottom: NSLayoutConstraint!
    @IBOutlet var bottomBackgroundLeading: NSLayoutConstraint!
    @IBOutlet var bottomBackgroundHeight: NSLayoutConstraint!
    @IBOutlet var imageViewWidth: NSLayoutConstraint!
    
    
    @IBOutlet var imageViewListBottom: NSLayoutConstraint!
    @IBOutlet var imageViewListHeight: NSLayoutConstraint!
    @IBOutlet var imageViewListWidth: NSLayoutConstraint!
    
    @IBOutlet var bottomBackgroundListLeading: NSLayoutConstraint!
    @IBOutlet var bottomBackgroundListHeight: NSLayoutConstraint!
    
    

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundView = UIView()
        self.selectedBackgroundView = UIView()
        
        self.backgroundView?.backgroundColor = UIColor.whiteColor()
//        self.selectedBackgroundView.backgroundColor = UIColor.lightGrayColor()
        
    }

    
}
