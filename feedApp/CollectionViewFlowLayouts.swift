//
//  CollectionViewFlowLayouts.swift
//  FeedApp
//
//  Created by Christoph Bohnen on 09.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit



// Motherclass
class FeedAppCollectionViewFlowLayout: UICollectionViewFlowLayout{
    
    init(size:CGSize){
        super.init()
        self.referenceSize = size
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    var flowLayoutType:Int?
    var referenceSize:CGSize!
    func setupLayout(){}
    
    func printOutSize(){
        print("frame from layout: \(referenceSize)")
    }
    

}


class GridViewFlowLayout:FeedAppCollectionViewFlowLayout {
    
    override init(size: CGSize) {
        super.init(size: size)
        self.setupLayout()
    }
    
    override func setupLayout() {
        self.itemSize = CGSize(width: ((self.referenceSize.width) / 2) - (ProductsCollectionView.Dimensions.GapWidth * 1.5), height: (self.referenceSize.height) / ProductsCollectionView.Dimensions.VisibleItemsInY)
        
        self.minimumLineSpacing = ProductsCollectionView.Dimensions.LineDistance
        self.minimumInteritemSpacing = 0
        self.flowLayoutType = 0
        self.sectionInset = UIEdgeInsets(top: ProductsCollectionView.Dimensions.SectionTopInset, left: ProductsCollectionView.Dimensions.GapWidth, bottom: ProductsCollectionView.Dimensions.SectionBottomInset, right: ProductsCollectionView.Dimensions.GapWidth)
    }
    
    
    override func invalidateLayout() {
        super.invalidateLayout()
        self.setupLayout()
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
}

class GridViewLandscapeFlowLayout:GridViewFlowLayout {
    override init(size: CGSize) {
        super.init(size: size)

        self.itemSize = CGSize(width:size.height, height: size.width)
//         switch height and witdth to make it work on landscape
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}



class ListViewFlowLayout:FeedAppCollectionViewFlowLayout {
    
    override init(size: CGSize) {
        super.init(size: size)
        self.setupLayout()
    }
    
    override func setupLayout() {
        
        self.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.itemSize = CGSize(width: referenceSize.width, height: 80)
        self.minimumInteritemSpacing = 0
        self.minimumLineSpacing = 1
        self.flowLayoutType = 1
        self.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    
    override func invalidateLayout() {
        super.invalidateLayout()
        self.setupLayout()
        
        
    }

    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



