//
//  DetailImagePageViewController.swift
//  FeedApp
//
//  Created by Christoph Bohnen on 07.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit

class DetailImagePageViewController: UIViewController {
    @IBOutlet var productImageView: UIImageView!

    let pageIndex:Int!
    let image:UIImage!
    let imageName:String!
    
    init(pageIndex:Int,image:UIImage,imageName:String) {
        self.pageIndex = pageIndex
        self.image = image
        self.imageName = imageName
        super.init(nibName: "DetailImageView", bundle: nil)
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        productImageView.image = image
        
    }
    
}
