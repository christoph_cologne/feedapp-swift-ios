//
//  MapResultDismissSegue.swift
//  mapview
//
//  Created by Christoph Bohnen on 12.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit

class MapResultDismissSegue: UIStoryboardSegue {
    override func perform() {
        let sourceVC = sourceViewController as! FeedAppMapResultsTableViewController
        let destinationVC = destinationViewController as! UINavigationController

        
        UIView.animateWithDuration(0.7, delay: 0, usingSpringWithDamping: 2, initialSpringVelocity: 1, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
            
            sourceVC.view.frame =  CGRect(x: sourceVC.view.frame.origin.x, y: destinationVC.view.frame.height, width: sourceVC.view.frame.width, height: sourceVC.view.frame.height)
            
            sourceVC.view.alpha = 0
            
            
            }, completion:
            {(completed) -> Void in
                sourceVC.removeFromParentViewController()
                
        })
        
        
        
    }
}
