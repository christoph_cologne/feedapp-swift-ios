//
//  FeedAppMapViewController.swift
//  FeedApp
//
//  Created by Christoph Bohnen on 22.06.15.
//  Copyright (c) 2015 Christoph Bohnen. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class FeedAppPin:NSObject, MKAnnotation {
    
    let title:String?
    let subTitle:String
    let coordinate:CLLocationCoordinate2D
    
    init(title:String,subTitle:String,coordinate:CLLocationCoordinate2D){
        self.title = title
        self.subTitle = subTitle
        self.coordinate = coordinate
    }
}


class FeedAppMapViewController: UIViewController,MKMapViewDelegate,CLLocationManagerDelegate {
    
    let homeAdress = "Kallbergstr. 85"

    
    @IBOutlet var calculateRouteButton: UIButton!
    @IBOutlet var mapView: MKMapView!
    
    // MARK: - Map Properties
    var mapTouchGestureRecognizer :UITapGestureRecognizer!
    var homeMapItem:MKMapItem!
    var destinationMapItem:MKMapItem!
    
    var homeRegion : CLCircularRegion!
    
    var initialLoad = true
    let geoCoder = CLGeocoder()
    let locationManager = CLLocationManager()
    
    
    var activityIndicator : FeedAppActivityIndicator!
    var alertController:UIAlertController!
    var declinedGeoAuthorizationAlert:UIAlertController!
    
    var informationLoaded = false
    
    // MARK: - ViewController Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        alertController = UIAlertController(title: NSLocalizedString("TRAVEL TIME", comment: "title for alert in MapView"), message: "THE TRAVEL TIME MESSAGE", preferredStyle: UIAlertControllerStyle.Alert)
        
        declinedGeoAuthorizationAlert = UIAlertController(title: "AUTHORIZATION", message: "NEED AUTHORIZATION", preferredStyle: UIAlertControllerStyle.Alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("CANCEL MAPVIEW ALERT CONTROLLER",comment: "cancel title mapview alert"), style: UIAlertActionStyle.Cancel, handler: nil)
        
        for view in [alertController.view, declinedGeoAuthorizationAlert.view]{
            view.tintColor = UIColor.blackColor()
        }
        
        
        declinedGeoAuthorizationAlert.addAction(cancelAction)
        alertController.addAction(cancelAction)
        
        
        self.title = "MapView"
        calculateRouteButton.layer.cornerRadius = calculateRouteButton.frame.width / 2
        calculateRouteButton.addTarget(self, action: "calculateRoute:", forControlEvents: UIControlEvents.TouchUpInside)
        view.bringSubviewToFront(calculateRouteButton)
        
        self.activityIndicator = FeedAppActivityIndicator(bounds: CGRect(x: (view.frame.width / 2) - 25, y: (view.frame.height / 2) - 25, width: 50, height: 50), color: UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.4))
        
        
        view.layer.addSublayer(self.activityIndicator)
        setupMapView()
        setupLocationManager()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Route Methods
    
    func calculateRoute(sender:AnyObject){
        
        
        if(!informationLoaded){

            self.activityIndicator.startAnimation()
            if(self.destinationMapItem != nil){
                
                let directionsRequest = MKDirectionsRequest()
                directionsRequest.source = self.homeMapItem
                directionsRequest.destination = self.destinationMapItem
                
                directionsRequest.requestsAlternateRoutes = true
                let directions = MKDirections(request: directionsRequest)
                directions.calculateDirectionsWithCompletionHandler { (response, error) -> Void in
                    
                    if(error != nil){
                        print(error)
                        
                    }
                    else{
                        //                         check for the route to distinguish it from the other with a different color
                        
                        var counter = 0
                        for route in response!.routes as! [MKRoute] {
                            
                            if(!self.mapView.overlays.isEmpty){
                                
                                self.mapView.insertOverlay(route.polyline, belowOverlay: self.mapView.overlays[0] as! MKOverlay)
                                
                            }
                            else{
                                self.mapView.addOverlay(route.polyline,
                                    level: MKOverlayLevel.AboveRoads)
                                
                                self.mapView.addAnnotation(MKPlacemark(placemark: self.destinationMapItem.placemark))
                                let routeRegion = self.coordinateRegionFromAnnotations(self.mapView.annotations as! [MKAnnotation])
                                
                                self.mapView.setRegion(routeRegion, animated: true)
                                
                                
                                
                                // alert setup
                                self.alertController.message = "Die Fahrt zum Standort dauert \(self.stringFromTimeInterval(route.expectedTravelTime))"
                                
                                self.presentViewController(self.alertController, animated: true, completion: nil)
                                self.informationLoaded = true
                                
                                self.activityIndicator.stopAnimation()
                                
                            }
                        }
                    }
                    
                }
                
            }
        }
        else{
            self.presentViewController(self.alertController, animated: true, completion: nil)
        }
    }
    
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer! {
        let renderer = MKPolylineRenderer(overlay: overlay)
        
        if(mapView.overlays.count == 1){
            renderer.strokeColor = UIColor(red: 0.3, green: 0.6, blue: 0.2, alpha: 1)
        }
        else{
            renderer.strokeColor = UIColor.lightGrayColor()
        }
        
        
        renderer.lineWidth = 4
        return renderer
    }
    
    
    private func stringFromTimeInterval(interval:NSTimeInterval) -> NSString {
        
        let ti = NSInteger(interval)
        let seconds = ti % 60
        let minutes = (ti / 60) % 60
        let hours = (ti / 3600)
        
        return NSString(format: "%0.2d:%0.2d:%0.2d",hours,minutes,seconds)
    }
    
    
    
    func requestLocationDataWithAdresse(adress:String,getFirstOnly:Bool,callback:([CLPlacemark])-> Void){
        
        
        self.geoCoder.geocodeAddressString(adress, completionHandler: { (placemarks, error) -> Void in
            
            var results : [CLPlacemark] = []
            
            if let placemarks = placemarks as [CLPlacemark]! {
                
                for var index = 0; index < placemarks.count; ++index{
                    let placemark = placemarks[index]
                    
                    results.append(placemark)
                    
                    callback(results)
                    if(getFirstOnly) {return}
                    
                }
                
            }
            
        })
        
        
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("locationManager error: \(error)")
        self.activityIndicator.stopAnimation()
        self.locationManager.stopUpdatingLocation()
        
        if(error.code == 0){
            alertController.message = NSLocalizedString("NOT WORKING ON SIMULATOR",comment: "explain user that this VC is not working on simulator")
            alertController.message = alertController.message! + NSLocalizedString("ASK TO ENABLE LOCATION IN SIMULATOR", comment: "ask to enable location in the simulator")
            self.presentViewController(alertController, animated: true, completion: nil)
            self.informationLoaded = true
            
        }
    }
    
    func locationManager(locationManager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.activityIndicator.stopAnimation()
        self.locationManager.stopUpdatingLocation()

        
        CLGeocoder().reverseGeocodeLocation(locationManager.location!, completionHandler: { (placemarks, error) -> Void in
            
            if(error != nil){
                print("locationManger error: \(error)")
                return
            }
            else{
                if(placemarks!.count > 0){
                    let placemark = placemarks![0] 
                    self.addPlaceMarkToMapView(placemark)
                    self.destinationMapItem = MKMapItem(placemark: MKPlacemark(placemark: placemark))
                    let destinationAnnotation = FeedAppPin(title: placemark.name!, subTitle: "\(placemark.thoroughfare), \(placemark.subThoroughfare)", coordinate: placemark.location!.coordinate)
                    
                    self.mapView.addAnnotation(destinationAnnotation)
                    
                    
                }
            }
            
        })
    }
    
    func coordinateRegionFromAnnotations(annotations:[MKAnnotation])-> MKCoordinateRegion{
        var rect = MKMapRectNull
        for annotation in annotations{
            let point = MKMapPointForCoordinate(annotation.coordinate)
            rect = MKMapRectUnion(rect, MKMapRectMake(point.x, point.y, 0, 0))
        }
        return MKCoordinateRegionForMapRect(rect)
    }
    
    
    
    
    //MARK: - Setup MapView
    func setupMapView(){
        self.mapView.delegate = self
        self.mapView.showsUserLocation = true
        self.mapView.scrollEnabled = true
        self.mapView.zoomEnabled = true
        self.mapView.pitchEnabled = true
        self.mapView.rotateEnabled = true
        
        mapTouchGestureRecognizer = UITapGestureRecognizer(target: self, action: "mapTouched:")
        mapView.addGestureRecognizer(mapTouchGestureRecognizer)
        
    }
    
    func addPlaceMarkToMapView(placemark:CLPlacemark){
        self.locationManager.stopUpdatingLocation()
        self.destinationMapItem = MKMapItem(placemark: MKPlacemark(placemark: placemark))
        
    }
    
    
    func mapTouched(sender:AnyObject){
        
        
    }
    
    func mapViewDidFinishRenderingMap(mapView: MKMapView, fullyRendered: Bool) {
        
        self.requestLocationDataWithAdresse(self.homeAdress,getFirstOnly: true,callback:{
            (placemark)-> Void in
            let placemark = placemark[0]
            
            let mapkitPlacemark = MKPlacemark(placemark: placemark)
            self.homeMapItem = MKMapItem(placemark: mapkitPlacemark)
            
            
            self.mapView.addAnnotation(MKPlacemark(placemark: placemark))
            
            let cl2d = CLLocationCoordinate2D(latitude: (placemark as CLPlacemark).location!.coordinate.latitude, longitude: (placemark as CLPlacemark).location!.coordinate.longitude)
            
            self.homeRegion = CLCircularRegion(center: cl2d, radius:100000, identifier: "homeRegion")
            
            let homeRegion = MKCoordinateRegionMakeWithDistance(cl2d, 10000, 10000)
            
            if (self.initialLoad){
                self.mapView.setRegion(homeRegion, animated: true)
                self.initialLoad = false
            }
            
            return
        })
        
        
        
    }
    
    
    
    //MARK: - Setup LocationManager
    func setupLocationManager(){
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
    }
    
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        if(status == CLAuthorizationStatus.Denied){
            self.declinedGeoAuthorizationAlert.message = "GEO DATA IS NECESSARY FOR THIS DEMO, PLEASE ACTIVATE"
            self.presentViewController(self.declinedGeoAuthorizationAlert, animated: true, completion: nil)

        }
        else if(status == CLAuthorizationStatus.Restricted){
            self.declinedGeoAuthorizationAlert.message = "GEO DATA IS NECESSARY FOR THIS DEMO, PLEASE ACTIVATE, NOTE AUTHORIZATION IS RESTRICTED"
            self.presentViewController(self.declinedGeoAuthorizationAlert, animated: true, completion: nil)
        }
        
        
    }
    
    //MARK: - Resign
    
    @IBAction func resignMapView(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    
}
