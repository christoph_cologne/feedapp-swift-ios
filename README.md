# Example iOS-Swift #

This Application **encompasses multiple subprojects** to show a breadth of different Frameworks in iOS Development.
_______

## The FeedApp Product Reminder ##
The FeedApp Product Reminder displays products with an image, title and small description inside a gridview or listview and allows the user to save appealing products and view them in a **UITableView** or inside its today widget in the **Notification Center**. The complete application uses **NSLocalizedString**s for i18n and is localized for **DE** and **EN**

### The CollectionView ###
   
  * Product data is being loaded from a server via **NSURLSession** in the **NetworkManager** as **JSON**.** (In case the server is not reachable the data is being loaded from a local JSON file)**
  * The **JSON** data is parsed via **NSJSONSerialization**
  * FlowLayout can be changed from a two column layout to a single column layout and vice versa (this allows an animated flowlayout change)
  * Demonstrates **UIRefreshControl**
  * Contains **UIImageView** and **UILabels**
  * Initial cell layout is set up in storyboard, 2 column layout in single **.xib** file. Both with **AutoLayout**
  * Price label demonstrates currency formatting with **NSNumberFormatter** 
    
 ---------------
   
### The DetailView ###

* Push Segue from CollectionView demonstrates a UIViewControllerAnimationTransition with UIScreen snapshots and **UIView animations**.
* Demonstrates a UIScrollView created in storyboard layed out with **AutoLayout** 
* Displays a paginated UIImageView realized with **UIPageViewController**
* Features a custom notifcation view
* Demonstrates **NSLocalNotification**
* Demonstrates persisting data in **NSUserDefaults**


------------

### The Today Widget ###

* Demonstrates launching of apps with **NSExtensionContext **
* Shows round imageViews achieved by CALayer's **cornerRadius** property
* Demonstrates a UICollectionView in a today widget

------------

## The Journey-time Calculator ##

Calculates the journey time from the creators home to the current position of the user

* Demonstrates **MKMapView**
* Demonstrates **LGeocoder**'s reverseGeocodeLocation
* Demonstrates calculating a route with **MKDirectionsRequest**
* Demonstrates adding **MKAnnotations**
* Demonstrates setting **MKMapView**'s region 
* Demonstrates **UIAlertController**

------------
## Bar & QR-Code Scanner ##

Scans a variety of different optical machine-readable representation of data and displays the data as string in a **UIAlerController**'s message.

* Demonstrates **AVCaptureSession**
* Demonstrates **AVCaptureMetadataOutputObjectsDelegate**
* Demonstrates CAShapeLayer with UIBezierpath for the focus indicator

-----------

## The Optical Character Scanner ##

An application that utilizes *Daniele Galiotto*'s Tesseract-OCR-iOS library to realize optical character scanning.

[https://github.com/gali8/Tesseract-OCR-iOS](link)

* The library target is added as framework to demonstrate **iOS8 frameworks**
* Demonstrates **AVCaptureStillImageOutput**
* Demonstrates the utilization of **NSOperationQueue**

----------

## The Spinner-View ##

An activity indicator in a form of a spinning arc.

* Demonstrates how to create arcs with **UIBezierPath**s
* Demonstrates how to use **CABasicAnimation** 

----------


## The Progress-Indicator-View 

A progress indicator view that closes an arc according to the current progress. Contains a **UILabel** to display the current progress in percent.

* Demonstrates how to animate the *strokeEnd* property of a **CAShapeLayer** with a **CABasicAnimation**
* Demonstrates a fadeToggle Animation realized with **UIView animations**

----------

## Slider Experiment ##

A **UISlider** instance that controls a **UIBezierPath**'s rotation to show a value on a scale